# Automation Testing HR System - Katalon Studio

HR System merupakan software berbasis website yang digunakan untuk sistem manajemen karyawan.
Fitur yang di test adalah sebagai berikut :

- edit bank akun karyawan.
- edit info personal karyawan.
- menampilkan, menambahkan dan mengedit pemberitahuan.
- menampilkan, menambahkan dan mengedit \*logistic support.

\*logistic support merupakan fitur untuk manajemen peminjaman barang kantor yang dibutuhkan oleh karyawan untuk kepentingan pekerjaan.

Test Scenario dan Manual test : https://docs.google.com/spreadsheets/d/1kxoNVJSUpiQaY777e1kmsbMp732PJOKvXAB6AfpmMsg/edit?usp=sharing
