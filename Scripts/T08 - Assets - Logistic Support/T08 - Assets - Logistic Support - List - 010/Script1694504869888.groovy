import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.doubleClick(findTestObject('Logistic Support/Table Head/th_Assign User'))

List<WebElement> listAssignUser = WebUI.findWebElements(findTestObject('Object Repository/Logistic Support/cols value/td_2 assign user'), 10)

String[] arrListAssignUser = new String[listAssignUser.size()]

String[] arrListAssignUserDesc = new String[listAssignUser.size()]

for (int i = 0; i < listAssignUser.size(); i++) {
    temp = listAssignUser.get(i).text

    (arrListAssignUser[i]) = temp

    (arrListAssignUserDesc[i]) = temp
}

Arrays.sort(arrListAssignUserDesc, Collections.reverseOrder())

assert arrListAssignUser == arrListAssignUserDesc