import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Logistic Support/input_search'), search)

WebDriver driver = DriverFactory.getWebDriver()
WebElement Table = driver.findElement(By.xpath('//table[@id="example23"]/tbody'))

List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

rows_count = rows_table.size()

for (int row = 0; row < rows_count; row++) {
	boolean contains = false
	List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))
	columns_count = Columns_row.size()
	
	for (int column = 0; column < columns_count; column++) {
		celltext = Columns_row.get(column).getText()
		// kalo mengandung kata kunci search maka contains = true (artinya data hasil searching match dengan kata kunci)
		if(celltext.contains(search)) {
			contains = true
			break;
		}
	}
	
	assert contains == true
}