import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.testng.Assert

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 001'), [:], FailureHandling.STOP_ON_FAILURE)

String downloadPath = 'C:\\Users\\Aslamadin Alvian Haz\\Downloads'

WebUI.click(findTestObject('Object Repository/Logistic Support/a_Excel'))

WebUI.delay(5)

Assert.assertTrue(isFileDownloaded(downloadPath, 'H R System (CI).xlsx'), 'Failed to download Expected document')

textExcel = getExcel('C:\\Users\\Aslamadin Alvian Haz\\Downloads\\H R System (CI).xlsx')

WebDriver driver = DriverFactory.getWebDriver()

List<String> pagination = driver.findElements(By.xpath('//div[@id="example23_paginate"]/span/a'))

page_count = Integer.parseInt(pagination.get(pagination.size()-1).getText())

for (int page = 0; page < page_count; page++) {
	WebElement Table = driver.findElement(By.xpath('//table[@id="example23"]/tbody'))

	List<String> rows_table = Table.findElements(By.tagName('tr'))

	rows_count = rows_table.size()

	for (int row = 0; row < rows_count; row++) {
		textTable = rows_table.get(row).getText().toString().replaceAll("\\s", "")
		
		assert textExcel.toString().replaceAll("\\s", "").contains(textTable)
	}
	
	WebUI.click(findTestObject('Object Repository/Notice Add and List/a_Next'))
}

File file = new File('C:\\Users\\Aslamadin Alvian Haz\\Downloads\\H R System (CI).xlsx')
file.delete() // remove this line if you want to keep the file

boolean isFileDownloaded(String downloadPath, String fileName) {
	long timeout = 5000
	long start = new Date().getTime()
	boolean downloaded = false
	File file = new File(downloadPath, fileName)
	while (!downloaded) {
		KeywordUtil.logInfo("Checking file exists ${file.absolutePath}")
		downloaded = file.exists()
		if (!downloaded) {
			long now = new Date().getTime()
			if (now - start > timeout) {
				break
			}
			Thread.sleep(3000)
		}
	}
	return downloaded
}

public getExcel(String path) {
	FileInputStream fis = new FileInputStream(path)
	
	Workbook workbook = WorkbookFactory.create(fis)
	
	Sheet sheet = workbook.getSheetAt(0)
	String text = ''
	for (Row row : sheet) {
		for (Cell cell : row) {
				
			if (cell.getCellType() == 1) {
				text += cell.getStringCellValue() + ""
			} else if (cell.getCellType() == 0) {
				text += Double.valueOf(cell.getNumericCellValue()).longValue() + ""
			}
	
		}
	}
	fis.close()
	
	return text
	}