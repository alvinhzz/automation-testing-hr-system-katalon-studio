import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Logistic Support/Table Head/th_Assets'))

List<WebElement> listAssets = WebUI.findWebElements(findTestObject('Logistic Support/cols value/td_1 assets'), 10)

String[] arrListAssets = new String[listAssets.size()]

String[] arrListAssetsDesc = new String[listAssets.size()]

for (int i = 0; i < listAssets.size(); i++) {
    temp = listAssets.get(i).text

    (arrListAssets[i]) = temp

    (arrListAssetsDesc[i]) = temp
}

Arrays.sort(arrListAssetsDesc, Collections.reverseOrder())

assert arrListAssets == arrListAssetsDesc

