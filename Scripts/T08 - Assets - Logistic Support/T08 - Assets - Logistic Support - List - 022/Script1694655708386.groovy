import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.scrollToElement(findTestObject('Logistic Support/Paging/a_Next'), 10)

WebDriver driver = DriverFactory.getWebDriver()

List<WebElement> pagination = driver.findElements(By.xpath('//div[@id="example23_paginate"]/span/a'))
expectedPage = Integer.parseInt(pagination.get(pagination.size()-1).getText())
halmn = Integer.parseInt(pagination.get(pagination.size()-1).getText())
String[] arrPagination = new String[halmn]
jmlHalaman = 0
int rows_count = 0

// berpindah halaman dan menghitung total halaman
for (int j = 0; j < halmn; j++) {
	jmlHalaman++
	
	WebElement Table = driver.findElement(By.xpath('//table[@id="example23"]/tbody'))
	
	List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
	rows_count += rows_table.size()
	
	WebUI.click(findTestObject('Object Repository/Logistic Support/Paging/a_Next'))	
}

HashMap<Integer, Object> dataPage = new HashMap()
dataPage.put('jmlHalaman', jmlHalaman)

String[] enteries = WebUI.findWebElement(findTestObject('Object Repository/Logistic Support/Paging/div_Showing 1 to 10 of 16 entries')).text.split(' ')
expectedRows = Integer.parseInt(enteries[5])

assert jmlHalaman == expectedPage
assert rows_count == expectedRows

return dataPage