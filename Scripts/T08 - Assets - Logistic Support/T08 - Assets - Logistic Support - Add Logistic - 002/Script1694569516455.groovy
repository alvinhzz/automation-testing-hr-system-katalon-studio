import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 001'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/span_Logistic List_arrow'), FailureHandling.STOP_ON_FAILURE)

logistic = WebUI.findWebElement(findTestObject('Logistic Support/Add Logistic Support/li_MacBook Pro 2023')).text

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/li_MacBook Pro 2023'))

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/span_Project_arrow'), FailureHandling.STOP_ON_FAILURE)

project = WebUI.findWebElement(findTestObject('Object Repository/Logistic Support/Add Logistic Support/li_Project X23')).text

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/li_Project X23'))

WebUI.selectOptionByLabel(findTestObject('Logistic Support/Add Logistic Support/select Task List'), 'Developing System', 
    false)

WebUI.selectOptionByLabel(findTestObject('Logistic Support/Add Logistic Support/select Task List'), task_list, false)

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/span_Employee Name_arrow'), FailureHandling.STOP_ON_FAILURE)

emp_name = WebUI.findWebElement(findTestObject('Logistic Support/Add Logistic Support/li_Will Williams')).text

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/li_Will Williams'))

WebUI.setText(findTestObject('Logistic Support/Add Logistic Support/input_Start Date_startdate'), start_date)

WebUI.setText(findTestObject('Logistic Support/Add Logistic Support/input_End Date_enddate'), end_date)

in_stock = WebUI.findWebElement(findTestObject('Object Repository/Logistic Support/Add Logistic Support/total stock')).text

WebUI.setText(findTestObject('Logistic Support/Add Logistic Support/input_Assign Qty_assignqty'), assgn_qty)

WebUI.setText(findTestObject('Logistic Support/Add Logistic Support/textarea_Remarks_remarks'), remarks)

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/button_Submit'))

WebUI.verifyElementPresent(findTestObject('Logistic Support/Add Logistic Support/div_Successfully Updated'), 5)

WebUI.verifyElementPresent(findTestObject('Logistic Support/h4_Logistic Support List'), 5)

List<String> inputList = new ArrayList<List>()

List<String> outputList = new ArrayList<List>()

String inputData = (((((((logistic + ' ') + emp_name) + ' ') + task_list.toString().substring(0, 13)) + ' ') + assgn_qty) + 
' ') + end_date

println(task_list.toString().substring(0, 13))

WebDriver driver = DriverFactory.getWebDriver()

List<String> pagination = driver.findElements(By.xpath('//div[@id="example23_paginate"]/span/a'))

page_count = pagination.size()

WebUI.delay(5)

for (int page = 0; page < page_count; page++) {
    WebElement Table = driver.findElement(By.xpath('//table[@id="example23"]/tbody'))

    boolean found = false

    List<String> rows_table = Table.findElements(By.tagName('tr'))

    rows_count = rows_table.size()

    for (int row = 0; row < rows_count; row++) {
        String text = rows_table.get(row).getText()

        println(text.toString().replace('...', ''))

        println(inputData)

        if (text.toString().replace('...', '').equalsIgnoreCase(inputData)) {
            WebUI.verifyMatch(text.toString().replace('...', ''), inputData, false)

            found = true

            break
        }
    }
    
    if (found == true) {
        break
    }
    
    WebUI.click(findTestObject('Object Repository/Logistic Support/Paging/a_Next'))
}

