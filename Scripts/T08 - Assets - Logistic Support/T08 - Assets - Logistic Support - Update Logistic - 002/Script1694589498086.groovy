import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Update Logistic - 001'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Logistic Support/Update/input_Refund Date_backdate'), refund_date)

WebUI.setText(findTestObject('Logistic Support/Update/input_Refund Qty_backqty'), refund_qty)

WebUI.click(findTestObject('Logistic Support/Update/button_Submit'))

WebUI.delay(5)

List<WebElement> listDataPos1 = WebUI.findWebElements(findTestObject('Object Repository/Logistic Support/Update/tble pos 1'), 
    10)

String[] arrDataPos1 = new String[listDataPos1.size()]

for (int cols = 0; cols < listDataPos1.size(); cols++) {
    (arrDataPos1[cols]) = listDataPos1.get(cols).text
}

assert refund_date == (arrDataPos1[5])

assert refund_qty == (arrDataPos1[6])

