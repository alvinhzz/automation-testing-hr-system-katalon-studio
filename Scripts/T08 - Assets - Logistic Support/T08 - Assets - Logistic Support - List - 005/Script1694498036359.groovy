import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.testautomationguru.utility.PDFUtil

import internal.GlobalVariable

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.testng.Assert as Assert

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 001'), [:], FailureHandling.STOP_ON_FAILURE)

String downloadPath = 'C:\\Users\\Aslamadin Alvian Haz\\Downloads'

WebUI.click(findTestObject('Logistic Support/a_PDF'))

WebUI.delay(2)

WebUI.switchToWindowIndex(0)

WebUI.delay(3)

//verivikasi apakah file terdownload atau tidak
Assert.assertTrue(isFileDownloaded(downloadPath, 'H R System (CI).pdf'), 'Failed to download Expected document') // remove this line if you want to keep the file

boolean isFileDownloaded(String downloadPath, String fileName) {
	long timeout = 5000

	long start = new Date().getTime()

	boolean downloaded = false

	File file = new File(downloadPath, fileName)

	while (!(downloaded)) {
		KeywordUtil.logInfo("Checking file exists $file.absolutePath")

		downloaded = file.exists()

		if (!downloaded) {
			long now = new Date().getTime()

			if ((now - start) > timeout) {
				break
			}
			
			Thread.sleep(3000)
		}
	}
	
	return downloaded
}

//verifikasi isi dalam file pdf apakah sesuai dengan table
PDFUtil pdfutil = new PDFUtil()
String pdfText = pdfutil.getText('C:\\Users\\Aslamadin Alvian Haz\\Downloads\\H R System (CI).pdf')

WebDriver driver = DriverFactory.getWebDriver()

List<String> pagination = driver.findElements(By.xpath('//div[@id="example23_paginate"]/span/a'))

page_count = Integer.parseInt(pagination.get(pagination.size()-1).getText())

for (int page = 0; page < page_count; page++) {
	WebElement Table = driver.findElement(By.xpath('//table[@id="example23"]/tbody'))

	List<String> rows_table = Table.findElements(By.tagName('tr'))

	rows_count = rows_table.size()

	for (int row = 0; row < rows_count; row++) {
		assert pdfText.contains(rows_table.get(row).getText())
	}
	
	WebUI.click(findTestObject('Object Repository/Notice Add and List/a_Next'))
}

File file = new File('C:\\Users\\Aslamadin Alvian Haz\\Downloads\\H R System (CI).pdf')
file.delete() // remove this line if you want to keep the file