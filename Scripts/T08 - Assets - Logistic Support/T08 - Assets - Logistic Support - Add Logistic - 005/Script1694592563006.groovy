import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 001'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/button_Submit'))

WebUI.verifyElementPresent(findTestObject('Logistic Support/error message/label_This field is required Logistic List'), 
    0)

WebUI.verifyElementPresent(findTestObject('Logistic Support/error message/label_This field is required Project'), 10)

WebUI.verifyElementPresent(findTestObject('Logistic Support/error message/label_This field is required Task List'), 10)

WebUI.verifyElementPresent(findTestObject('Logistic Support/error message/label_This field is required Employee Name'), 
    10)

WebUI.verifyElementPresent(findTestObject('Logistic Support/error message/error message Assign Qty field'), 10)

