import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 001'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/span_Logistic List_arrow'))

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/li_MacBook Pro 2023'))

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/span_Project_arrow'))

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/li_Project X23'))

WebUI.selectOptionByLabel(findTestObject('Logistic Support/Add Logistic Support/select Task List'), 'Developing System', 
    false)

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/span_Employee Name_arrow'))

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/li_Ilham God Basudara'))

WebUI.setText(findTestObject('Logistic Support/Add Logistic Support/input_Start Date_startdate'), '2023-10-05')

WebUI.setText(findTestObject('Logistic Support/Add Logistic Support/input_End Date_enddate'), '2023-09-28')

WebUI.setText(findTestObject('Logistic Support/Add Logistic Support/input_Assign Qty_assignqty'), '5')

WebUI.setText(findTestObject('Logistic Support/Add Logistic Support/textarea_Remarks_remarks'), 'keep it simple')

WebUI.click(findTestObject('Logistic Support/Add Logistic Support/button_Submit'))

WebUI.verifyTextPresent('Please enter a start date older than end date.', false)

