import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys
import org.openqa.selenium.WebElement

WebUI.callTestCase(findTestCase('T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 001'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.doubleClick(findTestObject('Logistic Support/Table Head/th_Back Qty'))

List<WebElement> listBackQty = WebUI.findWebElements(findTestObject('Object Repository/Logistic Support/cols value/td_7 back qty'), 10)

Integer[] arrListBackQty = new Integer[listBackQty.size()]

Integer[] arrListBackQtyDesc = new Integer[listBackQty.size()]

for (int i = 0; i < listBackQty.size(); i++) {
	int temp = 0
	if(listBackQty.get(i).text.equals("")) {
		temp = 0
	}else {
		temp = Integer.parseInt(listBackQty.get(i).text)
	}
	
	arrListBackQty[i] = temp

	arrListBackQtyDesc[i] = temp
}

Arrays.sort(arrListBackQtyDesc, Collections.reverseOrder())

assert arrListBackQty == arrListBackQtyDesc