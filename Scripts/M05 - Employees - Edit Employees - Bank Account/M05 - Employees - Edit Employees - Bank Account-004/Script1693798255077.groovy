import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('M05 - Employees - Edit Employees - Bank Account/M05 - Employees - Edit Employees - Bank Account-003'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Edit Bank Account/input_Bank Holder Name_holder_name'), holderName)

WebUI.setText(findTestObject('Edit Bank Account/input_Bank Name_bank_name'), bankName)

WebUI.setText(findTestObject('Edit Bank Account/input_Branch Name_branch_name'), branchName)

WebUI.setText(findTestObject('Edit Bank Account/input_Bank Account Number_account_number'), numberAccount)

WebUI.setText(findTestObject('Edit Bank Account/input_Bank Account Type_account_type'), typeAccount)

WebUI.click(findTestObject('Edit Bank Account/button_Save'))

WebUI.verifyElementPresent(findTestObject('Edit Bank Account/Successfully Updated'), 5)

WebUI.delay(7, FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Edit Bank Account/edit bank account button'))

// ambil text dari form halaman edit bank account
nameHolder = WebUI.getAttribute(findTestObject('Object Repository/Edit Bank Account/input_Bank Holder Name_holder_name'), 'value')
nameBank = WebUI.getAttribute(findTestObject('Object Repository/Edit Bank Account/input_Bank Name_bank_name'), 'value')
nameBranch = WebUI.getAttribute(findTestObject('Object Repository/Edit Bank Account/input_Branch Name_branch_name'), 'value')
accountNumber = WebUI.getAttribute(findTestObject('Object Repository/Edit Bank Account/input_Bank Account Number_account_number'), 'value')
accountType = WebUI.getAttribute(findTestObject('Object Repository/Edit Bank Account/input_Bank Account Type_account_type'), 'value')

assert holderName == nameHolder
assert bankName == nameBank
assert branchName == nameBranch
assert numberAccount == accountNumber
assert typeAccount == accountType
