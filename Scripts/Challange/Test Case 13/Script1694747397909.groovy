import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('Challange/Test Case 12'), [:], FailureHandling.STOP_ON_FAILURE)

List<WebElement> paginate = WebUI.findWebElements(findTestObject('Object Repository/TAJALAPAK/pAall paginate button'), 10)

last_page = Integer.parseInt(paginate.get(paginate.size() - 2).text)

for (page = last_page; page > 0; page--) {
    if(page==1) {
		WebUI.verifyElementNotClickable(findTestObject('Object Repository/TAJALAPAK/a_Previous'))
		break
	}
	
	
    WebUI.click(findTestObject('TAJALAPAK/a_Previous'))
	last_page--
}

first_page = WebUI.findWebElement(findTestObject('Object Repository/TAJALAPAK/a_1 halaman pertama')).text

assert last_page == Integer.parseInt(first_page)

