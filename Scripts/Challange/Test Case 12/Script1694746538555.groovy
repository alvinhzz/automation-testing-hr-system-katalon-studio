import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.thoughtworks.selenium.webdriven.WebDriverBackedSelenium

import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

WebUI.callTestCase(findTestCase('Challange/Test Case 2'), [:], FailureHandling.STOP_ON_FAILURE)

List<WebElement> paginate = WebUI.findWebElements(findTestObject('Object Repository/TAJALAPAK/pAall paginate button'), 10)
List<String> dataSebelum = new ArrayList<>()

last_page = Integer.parseInt(paginate.get(paginate.size()-2).text)
page_count = 0
total_data = 0

for(page = 0; page < last_page; page++) {
	List<WebElement> judul = WebUI.findWebElements(findTestObject('Object Repository/TAJALAPAK/td_2 judul berita'), 10)
	
	List <String> listJudul = new ArrayList<>()
	total_data += judul.size()
	
	for(int i = 0; i < judul.size(); i ++) {
		listJudul.add(judul.get(i).text)
	}
	page_count++
	
	if(page > 0) {
		assert listJudul == dataSebelum
		//		for(int a = 0; a < listJudul.size(); a++) {
		//			for(int b = 0; b < dataSebelum.size(); b++) {
		//						assert listJudul.get(a) != dataSebelum.get(b)
		//						println dataSebelum.get(b)
		//						println listJudul.get(a)
		//				}
		//			}
	}
	
	dataSebelum = listJudul
	
	if(page == last_page) {
		WebUI.verifyElementNotClickable(findTestObject('Object Repository/TAJALAPAK/a_Next Pagination'))
		break
	}
	
	WebUI.click(findTestObject('Object Repository/TAJALAPAK/a_Next Pagination'))
}

String[] totals_data = WebUI.findWebElement(findTestObject('Object Repository/TAJALAPAK/div_Showing 1 to 10 of 71 entries')).text.split(' ')

assert last_page == page_count
assert total_data == Integer.parseInt(totals_data[5])