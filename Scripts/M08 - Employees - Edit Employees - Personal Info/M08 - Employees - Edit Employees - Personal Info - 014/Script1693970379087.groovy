import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('M08 - Employees - Edit Employees - Personal Info/M08 - Employees - Edit Employees - Personal Info - 001'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Personal Info/input_NID Number_nid'), '12345')

WebUI.setText(findTestObject('Personal Info/input_Contact Number_contact'), '012345')

WebUI.click(findTestObject('Personal Info/button_Save'))

WebUI.verifyElementPresent(findTestObject('this field is require/label_Please enter at least 10 characters_NID'), 5)

WebUI.verifyElementPresent(findTestObject('this field is require/label_Please enter at least 10 characters_Contact'), 5)

