import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('M08 - Employees - Edit Employees - Personal Info/M08 - Employees - Edit Employees - Personal Info - 001'), 
    [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Personal Info/input_Employee PIN_eid'), '')

WebUI.setText(findTestObject('Personal Info/input_First Name_fname'), '')

WebUI.setText(findTestObject('Personal Info/input_Last Name_lname'), '')

WebUI.setText(findTestObject('Personal Info/input_Date Of Birth_dob'), '')

WebUI.setText(findTestObject('Personal Info/input_NID Number_nid'), '')

WebUI.setText(findTestObject('Personal Info/input_Contact Number_contact'), '')

WebUI.setText(findTestObject('Personal Info/input_Date Of Joining_joindate'), '')

WebUI.setText(findTestObject('Personal Info/input_Contract End Date_leavedate'), '')

WebUI.setText(findTestObject('Personal Info/input_Email_email'), '')

WebUI.click(findTestObject('Personal Info/button_Save'))

WebUI.verifyElementPresent(findTestObject('this field is require/label_This field is required_PIN'), 5)

WebUI.verifyElementPresent(findTestObject('this field is require/label_This field is required_firstname'), 5)

WebUI.verifyElementPresent(findTestObject('this field is require/label_This field is required_lastname'), 5)

WebUI.verifyElementPresent(findTestObject('this field is require/label_This field is required_dob'), 5)

WebUI.verifyElementPresent(findTestObject('this field is require/label_This field is required_nid'), 5)

WebUI.verifyElementPresent(findTestObject('this field is require/label_This field is required_contact'), 5)

WebUI.verifyElementPresent(findTestObject('this field is require/label_This field is required_doj'), 5)

WebUI.verifyElementNotPresent(findTestObject('this field is require/label_This field is required_Contract_end'), 5)

WebUI.verifyElementPresent(findTestObject('this field is require/label_This field is required_email'), 5)

