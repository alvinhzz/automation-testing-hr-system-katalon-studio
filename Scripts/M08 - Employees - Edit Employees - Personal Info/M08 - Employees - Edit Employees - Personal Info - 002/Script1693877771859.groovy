import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.text.SimpleDateFormat as SimpleDateFormat
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.support.ui.Select as Select

WebUI.callTestCase(findTestCase('M08 - Employees - Edit Employees - Personal Info/M08 - Employees - Edit Employees - Personal Info - 001'), 
    [:], FailureHandling.STOP_ON_FAILURE)

SimpleDateFormat sdf1 = new SimpleDateFormat('dd/MM/yyyy')
SimpleDateFormat sdf2 = new SimpleDateFormat('yyyy-MM-dd')

Date DOB = sdf1.parse(dob)
dateOfBirth = sdf2.format(DOB)

Date DOJ = sdf1.parse(dateJoin)
dateOfJoining = sdf2.format(DOJ)

Date DOE = sdf1.parse(dateEnd)
dateOfEnd = sdf2.format(DOE)

WebUI.setText(findTestObject('Personal Info/input_Employee PIN_eid'), pin)

WebUI.setText(findTestObject('Personal Info/input_First Name_fname'), firstName)

WebUI.setText(findTestObject('Personal Info/input_Last Name_lname'), lastName)

WebUI.selectOptionByValue(findTestObject('Personal Info/select_blood'), bloodGroup, false)

WebUI.selectOptionByValue(findTestObject('Personal Info/select_gender'), gender, false)

WebUI.selectOptionByValue(findTestObject('Personal Info/select_EMPLOYEE'), userType, false)

WebUI.selectOptionByValue(findTestObject('Personal Info/select_status'), status, false)

WebUI.setText(findTestObject('Personal Info/input_Date Of Birth_dob'), dateOfBirth, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Personal Info/input_NID Number_nid'), nidNumber)

WebUI.setText(findTestObject('Personal Info/input_Contact Number_contact'), contactNumber)

WebUI.selectOptionByLabel(findTestObject('Personal Info/select_departement'), departement, false)

WebUI.selectOptionByLabel(findTestObject('Personal Info/select_designition'), designition, false)

WebUI.setText(findTestObject('Personal Info/input_Date Of Joining_joindate'), dateOfJoining, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Personal Info/input_Contract End Date_leavedate'), dateOfEnd, FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Personal Info/input_Email_email'), email)

WebUI.click(findTestObject('Personal Info/button_Save'))

WebUI.delay(7)

pinEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/input_Employee PIN_eid'), 'value')

firstNameEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/input_First Name_fname'), 'value')

lastNameEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/input_Last Name_lname'), 'value')

bloodGroupEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/select_blood'), 'value')

genderEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/select_gender'), 'value')

typeEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/select_EMPLOYEE'), 'value')

statusEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/select_status'), 'value')

dobEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/input_Date Of Birth_dob'), 'value')

nidEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/input_NID Number_nid'), 'value')

contactEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/input_Contact Number_contact'), 'value')

Select selectDepEmp = new Select(DriverFactory.getWebDriver().findElement(By.xpath('//select[@name="dept"]')))

departementEmp = selectDepEmp.getFirstSelectedOption().getText()

Select selectDigEmp = new Select(DriverFactory.getWebDriver().findElement(By.xpath('//select[@name="deg"]')))

designitionEmp = selectDigEmp.getFirstSelectedOption().getText()

dojEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/input_Date Of Joining_joindate'), 'value')

doeEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/input_Contract End Date_leavedate'), 'value')

emailEmp = WebUI.getAttribute(findTestObject('Object Repository/Personal Info/input_Email_email'), 'value')

//Date dateDobEmp = sdf2.parse(dobEmp)
//
//dobParse = sdf1.format(dateDobEmp)
//
//Date dateDojEmp = sdf2.parse(dojEmp)
//
//dojParse = sdf1.format(dateDojEmp)
//
//Date dateDoeEmp = sdf2.parse(doeEmp)
//
//doeParse = sdf1.format(dateDoeEmp)

WebUI.verifyMatch(pin, pinEmp, false)

WebUI.verifyMatch(firstName, firstNameEmp, false)

WebUI.verifyMatch(lastName, lastNameEmp, false)

WebUI.verifyMatch(bloodGroup, bloodGroupEmp, false)

WebUI.verifyMatch(gender, genderEmp, false)

WebUI.verifyMatch(userType, typeEmp, false)

WebUI.verifyMatch(status, statusEmp, false)

WebUI.verifyMatch(dateOfBirth, dobEmp, false)

WebUI.verifyMatch(nidNumber, nidEmp, false)

WebUI.verifyMatch(contactNumber, contactEmp, false)

WebUI.verifyMatch(departement, departementEmp, false)

WebUI.verifyMatch(designition, designitionEmp, false)

WebUI.verifyMatch(dateOfJoining, dojEmp, false)

WebUI.verifyMatch(dateOfEnd, doeEmp, false)

WebUI.verifyMatch(email, emailEmp, false)

cardName = WebUI.findWebElement(findTestObject('Object Repository/Card/card_name')).text

cardDesignition = WebUI.findWebElement(findTestObject('Object Repository/Card/card_designition')).text

cardEmail = WebUI.findWebElement(findTestObject('Object Repository/Card/card_email')).text

cardPhone = WebUI.findWebElement(findTestObject('Object Repository/Card/card_phone')).text

fullname = ((firstNameEmp + ' ') + lastNameEmp)

WebUI.verifyMatch(cardName, fullname, false)

WebUI.verifyMatch(cardDesignition, designitionEmp, false)

WebUI.verifyMatch(cardEmail, emailEmp, false)

WebUI.verifyMatch(cardPhone, contactEmp, false)

