import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('M05 - Employees - Edit Employees - Bank Account/M05 - Employees - Edit Employees - Bank Account-001'), 
    [('username') : 'superadmin@mail.com', ('password') : 'p4y+y39Ir5Pc2g9xt3QkeQ=='], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Edit Bank Account/menu employee'))

WebUI.click(findTestObject('Edit Bank Account/submenu employee'))

WebDriver driver = DriverFactory.getWebDriver()

//mencari tahu jumlah halaman tabel
List<WebElement> pagination = driver.findElements(By.xpath('//div[@id="employees123_paginate"]/span/a'))
page_length = Integer.parseInt(pagination.get(pagination.size()-1).getText())

for(int page = 0; page < page_length; page++) { //perulangan untuk pindah halaman tabel
	found = false
	row_count = 1
	
	WebElement Table = driver.findElement(By.xpath('//table[@id="employees123"]/tbody'))
	List<String> rows_table = Table.findElements(By.tagName('tr')) //untuk ambil data tabel per baris kemudian dijadikan list
	println(rows_table.get(0).getText())
	row_size = rows_table.size()
	
	for (int row = 0; row < row_size; row++) {
		//untuk ambil data tabel per kolom berdasarkan baris yang sudah diambil kemudian dijadikan kolom
		List<String> Columns_row = rows_table.get(row).findElements(By.tagName('td'))
		columns_size = Columns_row.size()
	
		// kondisi jika nama yang dicari ketemu maka klik tombol edit employee
		if (Columns_row.get(0).getText().equalsIgnoreCase('Alvin venezuela')) {
			println(Columns_row.get(0).getText())
			// klik tombol edit employee
			Columns_row.get(columns_size-1).findElement(By.xpath("//table[@id='employees123']/tbody/tr[$row_count]/td[6]/a")).click()
			found = true
			break
		}
		row_count++
	}
	
	if(found == true) {
		break
	}
	
	// klik tombol next halaman tabel
	WebUI.click(findTestObject('Object Repository/Personal Info/a_Next'))
}

