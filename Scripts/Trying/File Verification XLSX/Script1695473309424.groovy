import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.FileInputStream

String xlsFilePath = 'C:\\Users\\Aslamadin Alvian Haz\\Downloads\\H R System (CI)(1).xlsx'

FileInputStream fis = new FileInputStream(xlsFilePath)

Workbook workbook = WorkbookFactory.create(fis)

Sheet sheet = workbook.getSheetAt(0)
String text = ''

row_size = sheet.getPhysicalNumberOfRows()

for(int i=1; i < row_size; i++) {
	RowCols = sheet.getRow(i)
	column_size = RowCols.getLastCellNum()
	for(int j = 0; j < column_size-1; j++) {
		cell = sheet.getRow(i).getCell(j)
		
		if(cell.getCellType() == 1) {
			println(cell.getStringCellValue())
		}else if(cell.getCellType() == 0) {
			println(cell.getNumericCellValue())
		}
	}
}

fis.close()