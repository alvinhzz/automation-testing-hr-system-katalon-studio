import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.openqa.selenium.Keys as Keys


class ReadExcel {
	public readExcelFile(String path) throws IOException {
		InputStream ExcelFileToRead = new FileInputStream(path)
		Workbook wb = new XSSFWorkbook(ExcelFileToRead)

		Sheet sheet = wb.getSheetAt(0)
		Row row
		Cell cell

		Iterator<Row> rows = sheet.rowIterator()
		
		String text = ''
		
		while (rows.hasNext()) {
			row = rows.next()
			Iterator<Cell> cells = row.cellIterator()

			while (cells.hasNext()) {
				cell = cells.next()

				if (cell.getCellType() == 1) {
					text += cell.getStringCellValue() + " "
				} else if (cell.getCellType() == 0) {
					text += Double.valueOf(cell.getNumericCellValue()).longValue() + " "
				} else {
					println('error')
				}
			}
		}
		return text
	}
}

ReadExcel excel = new ReadExcel()

text = excel.readExcelFile('C:\\Users\\Aslamadin Alvian Haz\\Desktop\\KATALON-TEST.xlsx')
println(text.toString().trim())