import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement

WebUI.callTestCase(findTestCase('T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 001'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Object Repository/Notice Add and List/th_Date'))

List<WebElement> dateNameList = WebUI.findWebElements(findTestObject('Object Repository/Notice Add and List/Tabel List/td_4'), 
    10)

String[] arrDateNameList = new String[dateNameList.size()]

String[] arrdateNameListAsc = new String[dateNameList.size()]

for (int i = 0; i < dateNameList.size(); i++) {
    temp = dateNameList.get(i).text

    (arrDateNameList[i]) = temp

    (arrdateNameListAsc[i]) = temp
}

Arrays.sort(arrdateNameListAsc)

assert arrDateNameList == arrdateNameListAsc
