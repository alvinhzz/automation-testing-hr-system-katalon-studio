import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.text.SimpleDateFormat as SimpleDateFormat
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - Add Notice - 001'), 
    [:], FailureHandling.STOP_ON_FAILURE)

SimpleDateFormat newFormat = new SimpleDateFormat('yyyy-MM-dd')
SimpleDateFormat oldFormat = new SimpleDateFormat('dd/MM/yyyy')

Date SETDATE = oldFormat.parse(setDate)
newSetDate = newFormat.format(SETDATE)

WebUI.setText(findTestObject('Notice Add and List/textarea_Notice Title_title'), setTitle)

WebUI.uploadFile(findTestObject('Notice Add and List/input_Title_file_url'), setImage)

WebUI.setText(findTestObject('Notice Add and List/input_Published Date_nodate (1)'), newSetDate)

WebUI.click(findTestObject('Notice Add and List/button_Submit'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Notice Add and List/th_Sl'))

WebUI.delay(5)

List<WebElement> post = WebUI.findWebElements(findTestObject('Object Repository/Notice Add and List/Tabel List/ambil baris paling atas'), 
    5)

cols = post.size()

String[] arrPost = new String[cols]

for (int i = 0; i < cols; i++) {
    temp = post.get(i).text

    (arrPost[i]) = temp
}



String[] imageName = setImage.split('\\\\')
inputNamePict = imageName[4].substring(0, imageName[4].size()-4)
outputNamePict = arrPost[2].replace('_', ' ').substring(0, imageName[4].size()-4)

assert setTitle == arrPost[1]

assert inputNamePict == outputNamePict

assert newSetDate == arrPost[3]