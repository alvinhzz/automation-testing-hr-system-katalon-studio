import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement

WebUI.callTestCase(findTestCase('T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 001'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Notice Add and List/input_search'), search)

WebDriver driver = DriverFactory.getWebDriver()

WebElement Table = driver.findElement(By.xpath('//table[@id="example23"]/tbody'))

List<String> rows_table = Table.findElements(By.tagName('tr'))

rows_count = rows_table.size()

for (int row = 0; row < rows_count; row++) {
    boolean contains = false

    List<String> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    columns_count = Columns_row.size()

    for (int column = 0; column < columns_count; column++) {
        celltext = Columns_row.get(column).getText()
		
		String[] keyword = search.toString().split(' ')
        // kalo mengandung kata kunci search maka contains = true (artinya data hasil searching match dengan kata kunci)
        if (celltext.toString().toLowerCase() .contains(keyword[0]).toString().toLowerCase()) {
            contains = true

            break
        }
    }
    
    assert contains == true
}

