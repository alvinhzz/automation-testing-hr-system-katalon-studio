import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.apache.poi.xssf.usermodel.XSSFSheet as XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook as XSSFWorkbook
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.testng.Assert as Assert

WebUI.callTestCase(findTestCase('T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 001'), [:], 
    FailureHandling.STOP_ON_FAILURE)

String downloadPath = 'C:\\Users\\Aslamadin Alvian Haz\\Downloads'

WebUI.click(findTestObject('Notice Add and List/a_CSV'))

WebUI.delay(5)

Assert.assertTrue(isFileDownloaded(downloadPath, 'H R System (CI).csv'), 'Failed to download Expected document')

List<String> data = readDataFromCSV('C:\\Users\\Aslamadin Alvian Haz\\Downloads\\H R System (CI).csv')

List<String> listTable = new ArrayList<List>()

List<String> listCsv = new ArrayList<List>()

WebDriver driver = DriverFactory.getWebDriver()

List<String> pagination = driver.findElements(By.xpath('//div[@id="example23_paginate"]/span/a'))

page_count = Integer.parseInt(pagination.get(pagination.size()-1).getText())

for (int page = 0; page < page_count; page++) {
    WebElement Table = driver.findElement(By.xpath('//table[@id="example23"]/tbody'))

    List<String> rows_table = Table.findElements(By.tagName('tr'))

    rows_count = rows_table.size()

    for (int row = 0; row < rows_count; row++) {
        listTable.add(rows_table.get(row).getText())
    }
    
    WebUI.click(findTestObject('Object Repository/Notice Add and List/a_Next'))
}

for (int i = 1; i < data.size(); i++) {
    temp = new String[data.size()]

    if (data.get(i) != null) {
        temp += (data.get(i) + ' ')
    }
    
    listCsv.add(temp.toString().replace('null', '').replace('"', '').replace(',', ''))
}

for (int iterasi; iterasi < listCsv.size(); iterasi++) {
    tempCsv = listCsv.get(iterasi).replace(' ', '')

    tempTable = listTable.get(iterasi).replace(' ', '').replace(',', '')

    assert tempCsv.contains(tempTable)
}

File file = new File('C:\\Users\\Aslamadin Alvian Haz\\Downloads\\H R System (CI).csv')
file.delete() //			file.delete() // remove this line if you want to keep the file

// di bawah ini kumpulan function : 1. untuk verifikasi download file berhasil, 2. untuk ambil isi file csv 

boolean isFileDownloaded(String downloadPath, String fileName) {
    long timeout = 5000

    long start = new Date().getTime()

    boolean downloaded = false

    File file = new File(downloadPath, fileName)

    while (!(downloaded)) {
        KeywordUtil.logInfo("Checking file exists $file.absolutePath")

        downloaded = file.exists()

        if (!downloaded) {
            long now = new Date().getTime()

            if ((now - start) > timeout) {
                break
            }
            
            Thread.sleep(3000)
        }
    }
    
    return downloaded
}

List<String> readDataFromCSV(String csvFile) {
    List<String> data = new ArrayList<List>()

    BufferedReader br = null

    String line = ''

    String csvSplitBy = ','

    try {
        br = new BufferedReader(new FileReader(csvFile))

        while ((line = br.readLine()) != null) {
            List<String> rowData = new ArrayList<List>()

            String[] values = line.split(csvSplitBy)

            for (String value : values) {
                rowData.add(value)
            }
            
            data.add(rowData)
        }
    }
    catch (Exception e) {
        e.printStackTrace()
    } 
    finally { 
        if (br != null) {
            br.close()
        }
    }
    
    return data
}

