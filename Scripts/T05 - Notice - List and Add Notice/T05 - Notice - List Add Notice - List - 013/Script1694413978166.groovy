import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.testautomationguru.utility.PDFUtil

import internal.GlobalVariable
import net.sf.jasperreports.export.TextExporterConfiguration

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.testng.Assert

WebUI.callTestCase(findTestCase('T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 001'), [:],
	FailureHandling.STOP_ON_FAILURE)

String downloadPath = 'C:\\Users\\Aslamadin Alvian Haz\\Downloads'

WebUI.click(findTestObject('Object Repository/Notice Add and List/a_PDF'))

WebUI.delay(3)

WebUI.switchToWindowIndex(0)

WebUI.delay(3)

Assert.assertTrue(isFileDownloaded(downloadPath, 'H R System (CI).pdf'), 'Failed to download Expected document')

String pdfPath = 'C:\\Users\\Aslamadin Alvian Haz\\Downloads\\H R System (CI).pdf'

PDFUtil pdfUtil = new PDFUtil()
String pdfText  = pdfUtil.getText(pdfPath)

WebDriver driver = DriverFactory.getWebDriver()

List<WebElement> pagination = driver.findElements(By.xpath('//div[@id="example23_paginate"]/span/a'))
page_count = pagination.size()

for (int page = 0; page < page_count; page++) {
	
	WebElement Table = driver.findElement(By.xpath('//table[@id="example23"]/tbody'))
	List<WebElement> rows_table = Table.findElements(By.tagName('tr'))
	rows_count = rows_table.size()
	
	for (int row = 0; row < rows_count; row++) {
		String Columns_row = rows_table.get(row).getText().trim()+ ''
		allRows = pdfText.toString().replace(" ", "")
		assert allRows.contains(Columns_row.replace(" ", ""))
	}
	WebUI.click(findTestObject('Object Repository/Notice Add and List/a_Next'))
}


File file = new File('C:\\Users\\Aslamadin Alvian Haz\\Downloads\\H R System (CI).pdf')
file.delete() // remove this line if you want to keep the file


boolean isFileDownloaded(String downloadPath, String fileName) {
	long timeout = 5000
	long start = new Date().getTime()
	boolean downloaded = false
	File file = new File(downloadPath, fileName)
	while (!downloaded) {
		KeywordUtil.logInfo("Checking file exists ${file.absolutePath}")
		downloaded = file.exists()
		if (!downloaded){
			long now = new Date().getTime()
			if (now - start > timeout) {
				break
			}
			Thread.sleep(3000)
		}
	}
	return downloaded
}