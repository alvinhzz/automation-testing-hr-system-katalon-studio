<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>T08 - Assets - Logistic Support Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>a52f4995-8bdc-4172-93a8-af9eb6842ca6</testSuiteGuid>
   <testCaseLink>
      <guid>7f7a85e2-5e60-4625-8fea-102e234e3c13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e76db9ea-022e-4c7e-85e1-12faeb70e110</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>461aff34-25a5-4610-b1b7-1665a26f93f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f6a1a44e-eaea-44be-9205-788939043aa7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 004</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fef8953c-d655-45fe-a1ff-ba1d769d2a92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 005</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d52bf854-066b-4f8f-84a7-58b5ae63958b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 006</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e8789cd6-3424-42a1-80a0-bd430401571b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 007</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a36e06ce-d1bd-4e3c-af5d-dd662e77d3b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 008</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a31ae350-b977-4bc6-8089-6d6eb74d6527</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 009</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>adedceea-7806-49df-955e-30899fed5840</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 010</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>70e94a0b-22ff-4627-b480-4e778bdcc48f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 011</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b22dccf0-5057-4ae8-ae5f-de0d145023ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 012</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2a997626-8eda-457c-a61a-0b9043176530</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 013</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4d24538e-b899-4ac3-a0c0-46a3283a6a01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 014</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>cc202cf8-f9ed-494a-96c4-eeb81bf6aa15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 015</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>c259b75f-39b9-48a3-81a8-8627ca431db5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 016</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ba398df1-3150-4b9d-b175-9833fa95cfd5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 017</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3a21f5ba-e263-424b-92fc-45900aa56bb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 018</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>f0be4d94-a26e-46cd-8c64-2631297fd0f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 019</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e1f102c5-0bfa-49e3-8fd2-6e04d1b7aa14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 020</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d315e504-4b21-4209-8636-2039af724ff4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 021</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d792275c-841c-4cb8-8f89-5bf7f0e484f4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b4572bd4-31b5-4288-a4ee-a852ce26eb2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 022</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e9481877-b4b6-4651-8f3b-0af79a9a2aa2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 023</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a11f9507-0dd8-4085-a58c-e5d75132b5b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - List - 024</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>90f977ef-141e-42e3-a239-2a937ffade24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d4640a64-472c-400d-a3c4-330aaea1d115</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ced20363-4d7f-4380-b88a-83b1e025696b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2be577f7-2976-4754-b113-e56a098140b8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ef4e3166-3935-4031-8b6d-3f8a3babc4f2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ca7bf1db-4dd3-4f10-83c4-c55839fe8ebf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ab4277e4-e602-4acb-84e8-9164796b6cfa</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f6cf1fe2-a399-4999-a75f-76c288f49c0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b3556d70-3d89-4da7-89f6-93cbbfb22787</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 004</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>e8db03a9-ff5e-4693-9607-9ff6a73b8618</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 005</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>684e2ad8-40b8-4ea6-abcc-3f86f533f653</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 006</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ae8295f5-1693-40a4-a95e-4354cd4169e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 007</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>417594e2-ef9f-44bd-af1e-e8ab71443b0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 008</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a3d70c58-6954-4212-be9f-984aa620f082</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 009</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>8116ec7a-a951-4480-ba7d-7ee3abb4f6c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Add Logistic - 010</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ced20363-4d7f-4380-b88a-83b1e025696b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2be577f7-2976-4754-b113-e56a098140b8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ef4e3166-3935-4031-8b6d-3f8a3babc4f2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ca7bf1db-4dd3-4f10-83c4-c55839fe8ebf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ab4277e4-e602-4acb-84e8-9164796b6cfa</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7b541e57-58f1-41be-bf97-024304431c0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Update Logistic - 001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>11dfb6ee-4867-4db6-8a14-220a154f0014</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Update Logistic - 002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c9f3e00d-6062-4be2-a56b-33acef8f2427</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c7c8e35a-f230-4867-8d5d-9b659fd65aa5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>925c4c74-ab82-4805-8588-c37c9396d30a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Update Logistic - 003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>612edb5d-850e-4833-9201-b59e455301a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Update Logistic - 004</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>cfee4c20-7ed4-4ca2-9fb7-d20322552305</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Update Logistic - 005</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>a567e84b-1739-45eb-995e-995f91a6b033</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T08 - Assets - Logistic Support/T08 - Assets - Logistic Support - Update Logistic - 006</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
