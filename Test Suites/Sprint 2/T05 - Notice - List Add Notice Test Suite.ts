<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>T05 - Notice - List Add Notice Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>96824ae5-f0d6-4d90-a0ce-e7822b26c835</testSuiteGuid>
   <testCaseLink>
      <guid>4e002913-e75a-4a4a-89fa-9847bc95f6d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>14478a11-8337-4e1d-ba0c-cc4f62f2b2a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>aec66155-dbca-4e5b-977f-04842e8c8168</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d2d723d9-cb6c-49e2-b85e-5037fcad9b47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 004</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>adc04277-2c05-4c78-a8b2-7374b337e5bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 005</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ebfa89ef-50e1-4051-8192-b31679a57264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 006</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>47c723bc-7c56-433c-b005-3510d3099036</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 007</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0e35fbc2-384f-4387-97dd-d6980b069aec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 008</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5faad529-9088-4331-8565-55a30d4eaad5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 009</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2ab32563-c87d-429e-a86a-be70be243303</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 010</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5a26182f-0f9a-4ada-94fe-7c18338dd391</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 011</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>03e39e80-c024-42b7-a067-209fcef33cf3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 012</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1825a73a-b394-448e-b509-43f29a020303</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 013</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>d9c0ae31-9965-475b-b9a9-698ede015d5d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 014</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fae9de3c-de02-45fd-961a-57e42b486b04</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 015</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3eeaec10-5963-4aca-9257-f9ea4d9adeb1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>dc297e0a-3bfa-4fc1-bb10-a5609818afff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 016</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>2c8e483c-61d5-4b8c-a7a6-3e00544a1237</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 017</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>fa8dd847-a3e0-43e2-9d8e-166f31d02428</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - Add Notice - 001</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>9b1adf4d-4e19-44bc-a881-ac110412be06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - Add Notice - 002</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>08c0bbea-0e91-41c3-a42a-151ab2a5087b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4542e0d5-a970-43e8-ab1c-c33cf6493200</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c2caa3e0-efcc-46e4-9634-67da58dbf7cb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d2d32d23-c493-4997-9dca-7dbfa9e845cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - Add Notice - 003</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>afa00a8c-25d5-4e45-99d2-cd738f77f790</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - Add Notice - 004</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>597edbe8-9c22-4863-9ca2-d5ba75bd313d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - Add Notice - 005</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>82edc199-9753-4df2-b16b-4e467c26ac59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - Add Notice - 006</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>7ef4dcd2-a462-4970-98f3-38332b7f9989</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - Add Notice - 007</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>ad5410ff-112b-4f5c-bbf3-45a7878fabda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - Add Notice - 008</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>af0a5c64-ec6a-491d-a9b8-430f3320071c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 018</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>b3c59ea0-0a56-41b8-ac78-5ec2b7110741</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 019</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>901398c0-4841-495c-8f5a-8d8ee569c017</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 020</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>aee5c44a-4b1c-464c-a2da-ba508335cd45</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>78e8a9cb-a8b9-4e86-934b-752bf2429866</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 021</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>76832b7c-2e24-4a54-b3ca-9575a2cd53e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/T05 - Notice - List and Add Notice/T05 - Notice - List Add Notice - List - 022</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f9601c7f-65db-4f12-80dd-01c00ef6f816</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
