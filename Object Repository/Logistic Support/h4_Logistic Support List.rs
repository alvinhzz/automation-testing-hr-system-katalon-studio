<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Logistic Support List</name>
   <tag></tag>
   <elementGuidId>91b45b77-f178-4ced-8325-d8bf028e8596</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h4.m-b-0.text-white</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-wrapper']/div/div[3]/div[2]/div/div/div/h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>b2e4c83e-e291-4cfa-84e5-1e0c9618423e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>m-b-0 text-white</value>
      <webElementGuid>4a7869ba-d175-47a4-9f93-b47a4ccf2656</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Logistic Support List</value>
      <webElementGuid>395ec575-efe6-4870-ae16-b7ae1c7a320c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-wrapper&quot;)/div[@class=&quot;page-wrapper&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;card card-outline-info&quot;]/div[@class=&quot;card-header&quot;]/h4[@class=&quot;m-b-0 text-white&quot;]</value>
      <webElementGuid>15992bb4-d892-4c31-808c-a5a105e2ce08</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-wrapper']/div/div[3]/div[2]/div/div/div/h4</value>
      <webElementGuid>6cd37344-1fc4-42cf-9277-5ce47c39e538</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logistic Support'])[3]/following::h4[1]</value>
      <webElementGuid>108f4501-0ebe-4cda-ac06-486c5eece183</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assets List'])[1]/following::h4[1]</value>
      <webElementGuid>35bfa11b-93cd-484b-a5b4-23cff206682a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy'])[1]/preceding::h4[1]</value>
      <webElementGuid>0863fd39-365d-46c6-861a-f365ea66de52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CSV'])[1]/preceding::h4[1]</value>
      <webElementGuid>343811ea-3a17-4db8-b3f8-0eb5e7aa7d62</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Logistic Support List']/parent::*</value>
      <webElementGuid>b522c41a-1898-4ada-b4c7-a85540711810</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/h4</value>
      <webElementGuid>046a4f31-98a4-4176-8edb-b89c79fe0441</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = ' Logistic Support List' or . = ' Logistic Support List')]</value>
      <webElementGuid>f945ad42-4e0d-4f6b-81a5-79069fdcc114</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
