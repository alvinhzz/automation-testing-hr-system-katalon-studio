<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Logistic Support</name>
   <tag></tag>
   <elementGuidId>981cff3f-169e-4965-be62-c13db6de1137</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.collapse.in > li:nth-of-type(3) > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//ul[@id='sidebarnav']/li[10]/ul/li[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>33514c1e-3f2b-499a-8421-592fc928000a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://10.9.2.63/hrsystem/Logistice/logistic_support</value>
      <webElementGuid>d4bca650-42fa-4f91-86c1-3cee0bc53f91</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Logistic Support </value>
      <webElementGuid>b97c5144-81dc-4990-bc91-3d325cc1ba06</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebarnav&quot;)/li[@class=&quot;active&quot;]/ul[@class=&quot;collapse in&quot;]/li[3]/a[1]</value>
      <webElementGuid>6a1a65e0-a2e6-4b87-807b-ff98cde55cc0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='sidebarnav']/li[10]/ul/li[3]/a</value>
      <webElementGuid>593badf6-bf41-43af-9b5c-6c0df03f06f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Logistic Support')]</value>
      <webElementGuid>78d969e9-1395-41eb-88d1-4248534401d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Asset List'])[1]/following::a[1]</value>
      <webElementGuid>d255dc31-b539-4fc3-8a16-7f21c96afac6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assets Category'])[1]/following::a[2]</value>
      <webElementGuid>bd9633a8-8b7d-47b6-b303-1a54c8cb141a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notice'])[1]/preceding::a[1]</value>
      <webElementGuid>65e7d7b7-6026-4245-99f0-f6829c7e6fa7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Settings'])[1]/preceding::a[2]</value>
      <webElementGuid>6e6f120f-4bf5-48bc-81c7-eda68e4c12df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Logistic Support']/parent::*</value>
      <webElementGuid>907cf04d-0051-4036-9096-71bce2b8b64b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://10.9.2.63/hrsystem/Logistice/logistic_support')]</value>
      <webElementGuid>ef053425-75b5-4f11-ad36-43822dbd5ee5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[10]/ul/li[3]/a</value>
      <webElementGuid>bb872cce-11c6-4f88-b9ba-d55ec1a33c8c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://10.9.2.63/hrsystem/Logistice/logistic_support' and (text() = ' Logistic Support ' or . = ' Logistic Support ')]</value>
      <webElementGuid>033f94b0-9948-49c5-8f70-b1849e415bac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
