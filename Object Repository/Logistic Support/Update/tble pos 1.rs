<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tble pos 1</name>
   <tag></tag>
   <elementGuidId>9b2da0e4-67dd-4d47-852b-9f2c6b4ae858</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='example23']/tbody/tr[1]/td</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>td.sorting_1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>c14fd05e-a251-4ced-a59e-8680c6e6329a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sorting_1</value>
      <webElementGuid>f6af1bf4-9c5f-459a-8c9b-d4f1bd004c1a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Laptop T10</value>
      <webElementGuid>e64a24d4-16e9-484e-b4b3-f0efecab80f9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23&quot;)/tbody[1]/tr[@class=&quot;odd&quot;]/td[@class=&quot;sorting_1&quot;]</value>
      <webElementGuid>0080590f-c5ea-4aa1-96ba-a56560c25c9a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='example23']/tbody/tr/td</value>
      <webElementGuid>ba15bbbe-9ef1-4f8e-a5a6-f0cbab2606b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::td[1]</value>
      <webElementGuid>2c0b9564-a506-46f5-96eb-62b0eac5109b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Back Qty'])[1]/following::td[1]</value>
      <webElementGuid>bb660ed6-1e48-40a5-baf3-d3b1e660f9ca</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tokek Gaul'])[1]/preceding::td[1]</value>
      <webElementGuid>30c03aa9-c313-4935-b7e4-b6fe7f77ab46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Astra Interna...'])[1]/preceding::td[2]</value>
      <webElementGuid>bb26408f-f384-4175-9e26-bdde5337fdab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Laptop T10']/parent::*</value>
      <webElementGuid>5efe31f9-07d9-4c71-96f4-81f91dfbc259</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td</value>
      <webElementGuid>9f25c0a6-3b3b-4bf8-9fec-ad819f619ee6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'Laptop T10' or . = 'Laptop T10')]</value>
      <webElementGuid>b1687f2e-773f-4958-964f-3fc3b946c296</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
