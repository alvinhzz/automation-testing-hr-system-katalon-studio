<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_2 assign user</name>
   <tag></tag>
   <elementGuidId>f9f5a9c9-1748-4484-8b5f-02046258b728</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td:nth-of-type(2)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='example23']/tbody/tr/td[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>9d9008fd-5f4c-44a3-97c2-61a54ef13a5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Mas Broo</value>
      <webElementGuid>31b743db-5e90-4c86-86a4-90aee326a04b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23&quot;)/tbody[1]/tr[@class=&quot;odd&quot;]/td[2]</value>
      <webElementGuid>bb5cdeb9-6077-424a-93bf-8fe93955d0e7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='example23']/tbody/tr/td[2]</value>
      <webElementGuid>d3d98d60-5ff0-4f53-9215-6754dacfb174</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MacBook Pro 2023'])[1]/following::td[1]</value>
      <webElementGuid>1d51c7dc-f2ac-4604-8c98-74445b5f87df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::td[2]</value>
      <webElementGuid>23578fce-e6ad-4ace-924e-0038087ba892</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='...'])[1]/preceding::td[1]</value>
      <webElementGuid>41c3aafc-13a4-4555-bd62-25120cb6175b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MacBook Pro 2023'])[2]/preceding::td[7]</value>
      <webElementGuid>461c03fa-9702-4dd2-b1ab-0914ff3de016</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]</value>
      <webElementGuid>fc4df84b-9561-4cdc-8035-d981825c6349</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'Mas Broo' or . = 'Mas Broo')]</value>
      <webElementGuid>d08b65c1-3f7e-44ec-a7b6-4489ff9b766b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
