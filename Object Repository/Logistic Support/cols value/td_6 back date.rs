<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_6 back date</name>
   <tag></tag>
   <elementGuidId>451c1b51-d5c1-414c-8c09-42ce1ac1026e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='example23']/tbody/tr/td[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>7eac8b2e-9e4a-4734-92f4-2a6ac76bc210</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23&quot;)/tbody[1]/tr[@class=&quot;odd&quot;]/td[7]</value>
      <webElementGuid>bd91c466-277c-433c-9486-46b08b21c7b6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='example23']/tbody/tr[3]/td[7]</value>
      <webElementGuid>ae5f0952-b2cf-4c0e-81b3-9cb39b2641a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//tr[3]/td[7]</value>
      <webElementGuid>06b1c849-8020-4d13-92ef-6a09110b7e91</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
