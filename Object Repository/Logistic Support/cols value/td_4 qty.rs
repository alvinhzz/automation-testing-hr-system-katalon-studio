<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_4 qty</name>
   <tag></tag>
   <elementGuidId>3277db5f-0dd3-468b-8650-59b8ff17647d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td:nth-of-type(4)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='example23']/tbody/tr/td[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>c0e06d4e-e9ce-441c-8d9a-55a5997477d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>aa37c42c-6465-49cb-8ab0-8b91e54601d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23&quot;)/tbody[1]/tr[@class=&quot;odd&quot;]/td[4]</value>
      <webElementGuid>1ab418c6-1779-482f-b32c-041844fe5312</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='example23']/tbody/tr/td[4]</value>
      <webElementGuid>64e2bc4f-0404-486f-973f-d1423c8312bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='...'])[1]/following::td[1]</value>
      <webElementGuid>255ad18a-70cf-47ce-9e23-ced84f7ed459</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mas Broo'])[3]/following::td[2]</value>
      <webElementGuid>fbb7e5aa-7f7f-4a74-931c-42e0f11a2068</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='MacBook Pro 2023'])[2]/preceding::td[5]</value>
      <webElementGuid>2f01a984-b1ac-4d61-b669-cb8d6d3263dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tom Belanda'])[1]/preceding::td[6]</value>
      <webElementGuid>9d96087e-bbad-4a79-ab4a-1cf6b4690595</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='3']/parent::*</value>
      <webElementGuid>2b0569ba-96a9-4532-83c4-7dc1b310a8ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[4]</value>
      <webElementGuid>bde6404f-3bea-43cb-b846-f25bafde4af4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '3' or . = '3')]</value>
      <webElementGuid>83259d36-11fc-4449-923b-4d90737ebd73</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
