<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Excel</name>
   <tag></tag>
   <elementGuidId>ff972f87-8bb8-4d4b-a8aa-9b66ba342183</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.dt-button.buttons-excel.buttons-html5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='example23_wrapper']/div/a[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>19bd3202-f930-43c5-b342-fbbf6edb2d65</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dt-button buttons-excel buttons-html5</value>
      <webElementGuid>94e58963-2b36-421c-87a0-42e247d914a6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>24f71c08-b434-493d-bf48-a2df642e5672</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>example23</value>
      <webElementGuid>8c66eaa1-d1b6-47e1-a4d0-4f0296530cd9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>35d9a252-f075-4c15-ae09-ca7e78fc1567</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Excel</value>
      <webElementGuid>69d3d9f1-852b-4978-acdf-c0b68cde0f59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23_wrapper&quot;)/div[@class=&quot;dt-buttons&quot;]/a[@class=&quot;dt-button buttons-excel buttons-html5&quot;]</value>
      <webElementGuid>453f6d80-a1ff-43d3-8869-e1dbdde8db5d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='example23_wrapper']/div/a[3]</value>
      <webElementGuid>50bed08f-4dae-45f0-8421-0a79269507a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CSV'])[1]/following::a[1]</value>
      <webElementGuid>4681b9e0-e231-437e-a87e-f86fa15c362e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy'])[1]/following::a[2]</value>
      <webElementGuid>16b7b139-9794-4dcb-816b-055ab6550246</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PDF'])[1]/preceding::a[1]</value>
      <webElementGuid>d2cb37ea-4414-4be5-bd70-1bcc905e47d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[13]</value>
      <webElementGuid>fbdfbe49-f3bd-4a05-beb9-b8e2c927b8d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//a[3]</value>
      <webElementGuid>01e76ed4-1022-458b-9d29-e9243b002cc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'Excel' or . = 'Excel')]</value>
      <webElementGuid>3a164132-7f3a-4c06-83d5-37a89f3f79d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
