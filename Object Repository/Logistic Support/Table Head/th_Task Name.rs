<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>th_Task Name</name>
   <tag></tag>
   <elementGuidId>f0d120c3-7ab2-48db-8185-fe6e7bd29e64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='example23']/thead/tr/th[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>th</value>
      <webElementGuid>08ae4eba-f2f1-45be-bc0e-c100d6fea18d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sorting</value>
      <webElementGuid>e1c514d8-a667-4d93-a4be-62334130177b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>b475a7f9-a7bd-4f0d-92da-1ca1d27328ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>example23</value>
      <webElementGuid>57c548ff-f8e5-4a14-9b6d-b7629bd6da53</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rowspan</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>caed3c10-ac51-494f-88e3-865786f0f9ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>colspan</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>76ca9475-abbb-4e22-abb2-2b415572738a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Task Name: activate to sort column ascending</value>
      <webElementGuid>b926e169-47d1-4b66-b4ac-c583f7f56c48</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Task Name</value>
      <webElementGuid>815dcd2c-975e-4a3b-8810-81bf13cdef8c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23&quot;)/thead[1]/tr[1]/th[@class=&quot;sorting&quot;]</value>
      <webElementGuid>435ae572-665a-4a1f-8665-9f0d40feafb1</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='example23']/thead/tr/th[3]</value>
      <webElementGuid>8e1fc975-c5a1-431c-b9a6-94fedc768472</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assign User'])[1]/following::th[1]</value>
      <webElementGuid>4a9e9f75-3b41-4501-8d31-5a9a763b0d63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assets'])[2]/following::th[2]</value>
      <webElementGuid>a4cb354d-c1ae-47b8-b295-5ff2b882671c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Qty'])[1]/preceding::th[1]</value>
      <webElementGuid>7f80fad6-9a46-431a-93a7-338a90958264</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='End Date'])[1]/preceding::th[2]</value>
      <webElementGuid>5e3a987f-be5d-4331-a4c5-2ad43882795a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Task Name']/parent::*</value>
      <webElementGuid>2b83d1d3-5601-4a34-a2ba-59d90cf526cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//th[3]</value>
      <webElementGuid>89e99fe2-d6e6-4d85-928b-69f0e98e43d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//th[(text() = 'Task Name' or . = 'Task Name')]</value>
      <webElementGuid>0965a15c-5955-40d7-88dc-12e2dfdf96fe</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
