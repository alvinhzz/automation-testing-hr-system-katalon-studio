<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_In Stock</name>
   <tag></tag>
   <elementGuidId>c0cfe30c-82d8-45fa-af40-9dcc86406921</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-md-6 > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='logisticsform']/div/div/div[2]/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>eb9bef82-9a8c-4946-b7ed-739007cb4a00</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>In Stock: </value>
      <webElementGuid>cbbd645b-1ec9-4287-8954-c1d29745cf8b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;logisticsform&quot;)/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/span[1]</value>
      <webElementGuid>e6ef12c8-7f9c-4ab6-804c-d59fa23f4800</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='logisticsform']/div/div/div[2]/span</value>
      <webElementGuid>94325f20-96ad-42c7-b515-01c4f17592fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='End Date'])[2]/following::span[1]</value>
      <webElementGuid>e1c43e4f-f40e-4832-bebd-3c5fbdb318ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Start Date'])[1]/following::span[1]</value>
      <webElementGuid>9768f597-a0c7-400b-aa55-031a409f6500</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assign Qty'])[1]/preceding::span[1]</value>
      <webElementGuid>ac4b51e7-f6b2-497a-bdb4-90fcac1b27b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks'])[1]/preceding::span[1]</value>
      <webElementGuid>4b034b84-9b2f-49bc-9414-275555404a77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='In Stock:']/parent::*</value>
      <webElementGuid>c639aad8-c1ac-44a2-bbd2-25f94c8cf369</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/span</value>
      <webElementGuid>4bc3d1e7-2215-442a-987b-03b81b494129</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'In Stock: ' or . = 'In Stock: ')]</value>
      <webElementGuid>246c8200-7e26-4f0f-86ef-f83938267f0a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
