<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_MacBook Pro 2023</name>
   <tag></tag>
   <elementGuidId>7b7e16c7-9c48-47d3-a8e5-a31e57939692</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//li[@role = 'treeitem' and (text() = 'MacBook Pro 2023' or . = 'MacBook Pro 2023')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#select2-logid-vh-result-88qw-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='select2-logid-vh-result-88qw-2']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>6bcfbc8d-44e2-4c65-b2ea-a358cdf42dc5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2-results__option select2-results__option--highlighted</value>
      <webElementGuid>18ae09b8-1888-4067-8141-8a474cd19a44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>select2-logid-vh-result-88qw-2</value>
      <webElementGuid>936bbcf8-0d40-468d-9da6-6e880a842a6e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>treeitem</value>
      <webElementGuid>fda9e27c-91c3-462a-9882-0dd29d230305</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>39bc4078-8d01-40f7-abaa-c62e9d927729</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>MacBook Pro 2023</value>
      <webElementGuid>56561aa9-dccc-41ec-9ad2-8a56ad8178c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;select2-logid-vh-result-88qw-2&quot;)</value>
      <webElementGuid>e0ae6677-b2fa-472d-943f-89e7ec0cf0b7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='select2-logid-vh-result-88qw-2']</value>
      <webElementGuid>577c853a-c515-401b-b5b1-53b37dd3f2d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='select2-logid-vh-results']/li[2]</value>
      <webElementGuid>67e28541-d182-47c3-ab58-94cb18f5575b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Laptop T10'])[3]/following::li[1]</value>
      <webElementGuid>ba11a46f-940e-4ef4-9926-4f2c8e0a6da6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('id(', '&quot;', 'select2-logid-vh-result-88qw-2', '&quot;', ')')])[1]/following::li[2]</value>
      <webElementGuid>0c4e9f40-f464-49e6-90c5-683774fa0071</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Scaner Absen'])[5]/preceding::li[1]</value>
      <webElementGuid>8259847c-1cd8-4e7e-acbf-0d3981e34715</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/ul/li[2]</value>
      <webElementGuid>63d7597d-aa39-4e63-9ee4-dd3a5cca856a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'select2-logid-vh-result-88qw-2' and (text() = 'MacBook Pro 2023' or . = 'MacBook Pro 2023')]</value>
      <webElementGuid>1c9f5154-2155-44d4-823a-ee3ef9b3f335</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
