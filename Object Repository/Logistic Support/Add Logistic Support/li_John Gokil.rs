<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_John Gokil</name>
   <tag></tag>
   <elementGuidId>0f932beb-c45f-46ad-8e8e-e4ac2e9d3bee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#select2-assignval-result-6cob-Smi1266</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='select2-assignval-result-6cob-Smi1266']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>5b1af73a-8949-4ede-a2b9-2e9768ef889f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>select2-results__option select2-results__option--highlighted</value>
      <webElementGuid>7384b0b8-0bea-4ee5-afcb-0358a4232c0a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>select2-assignval-result-6cob-Smi1266</value>
      <webElementGuid>4fd876c6-2453-4e20-96a2-5c325fbbba01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>treeitem</value>
      <webElementGuid>d2b357ce-7d22-45eb-944e-1a5d3355c476</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-selected</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>bacd8fba-5696-4af4-864e-5c46a89ee2b8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>John Gokil</value>
      <webElementGuid>1b1d8abe-73bf-401c-8b44-4b80eb17576b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;select2-assignval-result-6cob-Smi1266&quot;)</value>
      <webElementGuid>f167ac1c-0641-40ca-acf5-8cc765c118b0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//li[@id='select2-assignval-result-6cob-Smi1266']</value>
      <webElementGuid>4a052c25-5d8c-4128-be63-1789915e8a2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//ul[@id='select2-assignval-results']/li[4]</value>
      <webElementGuid>a417f59f-cf15-47f1-8af3-78873f2844b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tom Belanda'])[3]/following::li[1]</value>
      <webElementGuid>780e4004-6b6b-4624-b1a6-20207107fd96</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='John Greenwood'])[8]/following::li[2]</value>
      <webElementGuid>53eab098-0af6-47d0-931e-8ee037dcb6c3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ilham God Basudara'])[7]/preceding::li[1]</value>
      <webElementGuid>b19564ce-af84-4f21-a4f6-849af1d04da9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]/ul/li[4]</value>
      <webElementGuid>d29d8de4-2d5d-4b4f-8b66-28848c1948d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[@id = 'select2-assignval-result-6cob-Smi1266' and (text() = 'John Gokil' or . = 'John Gokil')]</value>
      <webElementGuid>108ee05c-68ce-49c2-82d1-b71898908237</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
