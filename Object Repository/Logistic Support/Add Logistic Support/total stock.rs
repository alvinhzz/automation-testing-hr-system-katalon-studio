<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>total stock</name>
   <tag></tag>
   <elementGuidId>13a9f6e6-a4a1-4bde-96be-9e1fb2cb06f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='logisticsform']/div/div/div[2]/span/div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = '     22' or . = '     22')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.qty</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>318fe417-70e7-43da-9797-f79421d88a74</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>qty</value>
      <webElementGuid>b0193c51-9a94-4f8b-849d-db1bfe73ec21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>     22</value>
      <webElementGuid>c3e723e6-26a2-4514-88e9-971e1db7d895</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;logisticsform&quot;)/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/span[1]/div[@class=&quot;qty&quot;]</value>
      <webElementGuid>b9a8f0b9-c60d-4fcb-be4a-891a5e7dab9c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='logisticsform']/div/div/div[2]/span/div</value>
      <webElementGuid>ed7abe39-e054-4354-abf2-95cc19447f15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='End Date'])[2]/following::div[1]</value>
      <webElementGuid>25d86a45-a84d-471b-9c9f-699c9d09534c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assign Qty'])[1]/preceding::div[1]</value>
      <webElementGuid>80902907-16f5-4e93-b3a0-979a1d756480</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks'])[1]/preceding::div[2]</value>
      <webElementGuid>bb7201d0-b225-4b71-9a20-c0ffeaba074d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='22']/parent::*</value>
      <webElementGuid>ef0954a0-89be-4ca7-a28d-f4fa2c50c284</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div</value>
      <webElementGuid>76d366af-06a9-4e20-91fa-c7ea9f04909b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '     22' or . = '     22')]</value>
      <webElementGuid>629e27e6-8d44-4108-a7a5-6d3f43fba254</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
