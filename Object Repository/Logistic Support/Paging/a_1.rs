<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_1</name>
   <tag></tag>
   <elementGuidId>5a014f92-4230-4d13-ac5b-d91086793e9e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.paginate_button.current</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='example23_paginate']/span/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5d36e15b-4354-4e39-a515-fe04e5183189</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>paginate_button current</value>
      <webElementGuid>9c936393-618b-4d26-9e2e-27cccb0647fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>example23</value>
      <webElementGuid>12051973-164d-4ba0-a01f-46970c84c443</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dt-idx</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>6b7f61b2-e08f-4a31-a04b-86d0d1bb1d30</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>3d59cf4f-0028-4ba3-88c1-364eac251726</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>7dfa1c89-ab8c-4c12-8007-1ae1acd1cbcd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23_paginate&quot;)/span[1]/a[@class=&quot;paginate_button current&quot;]</value>
      <webElementGuid>b81a5db6-b7e4-482f-8fa7-4ba899c84e8d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='example23_paginate']/span/a</value>
      <webElementGuid>8a15c251-4713-445c-b1a4-99a1882657c8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'1')]</value>
      <webElementGuid>29643f6d-188b-40ea-874a-7b9b8f847460</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous'])[1]/following::a[1]</value>
      <webElementGuid>ce75ac25-f501-4d74-93a2-cd96014833f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Showing 1 to 10 of 16 entries'])[1]/following::a[2]</value>
      <webElementGuid>07453f98-bd0d-42ed-9373-0928552d290f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Next'])[1]/preceding::a[2]</value>
      <webElementGuid>a394320c-f620-44c4-b19f-f9ad02ef86cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Logistice'])[1]/preceding::a[3]</value>
      <webElementGuid>a0fed31d-ee49-40b9-8362-e042492f8224</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/a</value>
      <webElementGuid>4c36eefe-894a-4a91-a726-a24ac4f897ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[(text() = '1' or . = '1')]</value>
      <webElementGuid>2d4440c9-ca36-4506-81bb-fc3a47c8ba37</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
