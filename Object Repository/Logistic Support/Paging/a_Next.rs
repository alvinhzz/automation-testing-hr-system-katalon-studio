<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Next</name>
   <tag></tag>
   <elementGuidId>65e1ef4f-4d69-441b-bacd-89aa23c50405</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#example23_next</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='example23_next']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0b482db6-469e-4171-8024-b8bbc03ddffb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>paginate_button next</value>
      <webElementGuid>8b73462a-26ae-4acb-b58d-6ef3dc0598e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>example23</value>
      <webElementGuid>b7fee93c-c384-42b1-8fd6-07ba58714a96</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dt-idx</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>17aa856f-6c60-4555-a981-b58fe96d5fb5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>e5d5de33-eab1-42f1-8c8a-b918f0e1b30a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>example23_next</value>
      <webElementGuid>d4bded3e-80ae-4ae2-a5a7-50ab1d793e0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Next</value>
      <webElementGuid>b7bb40cc-7ef7-4fb4-a0b3-4efa03e20fcf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23_next&quot;)</value>
      <webElementGuid>7b9e9b17-5ea0-46aa-a144-2e0215aa339a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='example23_next']</value>
      <webElementGuid>4acc35f6-5745-49ba-b485-1279b750393f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='example23_paginate']/a[2]</value>
      <webElementGuid>c257599b-0b76-4da3-9372-78a64702e18e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Next')]</value>
      <webElementGuid>68939e75-2b75-424e-bb26-e0e8ccfc26fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous'])[1]/following::a[3]</value>
      <webElementGuid>7a8c31db-4802-4c26-8066-8fe1bc31da65</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Showing 1 to 10 of 16 entries'])[1]/following::a[4]</value>
      <webElementGuid>50394b54-3ff0-480c-ba3b-f3d1aa87cb69</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Logistice'])[1]/preceding::a[1]</value>
      <webElementGuid>1602cf03-e49b-470d-9044-b17668c7000d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logistic List'])[1]/preceding::a[1]</value>
      <webElementGuid>b21a5166-a6d3-4c35-99da-e291cab5ce81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next']/parent::*</value>
      <webElementGuid>fbbceee4-f274-4658-8892-f1d653b6363d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/a[2]</value>
      <webElementGuid>f4ae2425-fc0b-4273-b3c7-a7aeca1f86a9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'example23_next' and (text() = 'Next' or . = 'Next')]</value>
      <webElementGuid>a2340f18-2451-4575-8c40-5908cfc4c8c3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
