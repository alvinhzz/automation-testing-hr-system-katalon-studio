<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Assets List</name>
   <tag></tag>
   <elementGuidId>996baf32-8124-445a-aa2c-0fb9dba0ed8c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h4.m-b-0.text-white</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-wrapper']/div/div[3]/div[2]/div/div/div/h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>d5eb8dca-2027-4b04-9f00-af386a4c5487</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>m-b-0 text-white</value>
      <webElementGuid>abc691bd-70cf-40ab-bd78-d9b12392d2a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Assets List</value>
      <webElementGuid>c33100dc-27d7-4fc1-8077-0a17699c7a9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-wrapper&quot;)/div[@class=&quot;page-wrapper&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;card card-outline-info&quot;]/div[@class=&quot;card-header&quot;]/h4[@class=&quot;m-b-0 text-white&quot;]</value>
      <webElementGuid>67c09d90-fddb-4fc0-8153-5ade2df8f056</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-wrapper']/div/div[3]/div[2]/div/div/div/h4</value>
      <webElementGuid>c92d7289-e7e2-428d-86f8-6a7536efb9d5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logistic Support'])[2]/following::h4[1]</value>
      <webElementGuid>717e55cf-701b-4949-a6f7-d21c6f999ea6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logistic Support List'])[1]/following::h4[1]</value>
      <webElementGuid>3565c1bf-4f84-40ac-9d52-14e41323dafc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy'])[1]/preceding::h4[1]</value>
      <webElementGuid>8cffede3-56c8-4a3f-b15c-9b67001c59b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CSV'])[1]/preceding::h4[1]</value>
      <webElementGuid>17d35187-7eef-4af5-800b-0e2f2c853601</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/h4</value>
      <webElementGuid>7a8400ad-453e-4c17-9461-5be6495aba5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = ' Assets List' or . = ' Assets List')]</value>
      <webElementGuid>2034e342-e3dd-4df5-8f08-2b62a45fb180</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
