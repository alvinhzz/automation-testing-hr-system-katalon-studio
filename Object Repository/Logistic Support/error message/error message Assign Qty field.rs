<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>error message Assign Qty field</name>
   <tag></tag>
   <elementGuidId>f299f1cb-76fb-4b09-985e-f3f312d44ffb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='logisticsform']/div/div/div[2]/div[3]/label[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>446ac1e7-0185-441b-b6d2-138598d2a05f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>recipient-name1 qty</value>
      <webElementGuid>0070c35e-51dc-48c4-a2a9-73506924bacd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>generated</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>aeb1d42f-75c5-402b-8649-34c2cc0a96b9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>error</value>
      <webElementGuid>5f22e892-268f-419a-aeba-6e0c3b00a3c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Please enter a value less than or equal to 16.</value>
      <webElementGuid>81cdecd6-fafb-4a3b-9ef8-7bad32436973</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;logisticsform&quot;)/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;form-group&quot;]/label[@class=&quot;error&quot;]</value>
      <webElementGuid>00e9ca12-cfdd-4458-be77-083e7e519b93</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='logisticsform']/div/div/div[2]/div[3]/label[2]</value>
      <webElementGuid>a7be11eb-3d8e-45dd-a010-564b668dc6dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Assign Qty'])[1]/following::label[1]</value>
      <webElementGuid>204670ca-d1d3-43e6-9d95-b59ae236d35e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Remarks'])[1]/preceding::label[1]</value>
      <webElementGuid>65bbdc3a-28a1-40ff-aeb9-8c6af65cd165</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/preceding::label[2]</value>
      <webElementGuid>fbd83c37-dd0c-434a-ba3a-2bc1334061b1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Please enter a value less than or equal to 16.']/parent::*</value>
      <webElementGuid>fd76b703-7938-46c4-8cf7-b6496c6073e1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/label[2]</value>
      <webElementGuid>bf68f043-11bb-4a4f-a90a-bab9dac24d13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Please enter a value less than or equal to 16.' or . = 'Please enter a value less than or equal to 16.')]</value>
      <webElementGuid>ea9e8dcf-d3ad-4c76-aef8-9663f4d58d57</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
