<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_This field is required Logistic List</name>
   <tag></tag>
   <elementGuidId>36568bd3-2bc4-451d-920a-70c01f29b97c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='logisticsform']/div/div/div/div[1]/label[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>label.error</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>9867b332-50a8-4192-a729-8836d40dc409</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>logid</value>
      <webElementGuid>fcfc6fc4-ac53-4de7-bcb0-ce3f8a038111</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>generated</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>19ba24a4-6f23-4062-8e2e-f400b35c6212</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>error</value>
      <webElementGuid>406d870f-ed2c-46f9-9751-3c4e177c7ee5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>This field is required.</value>
      <webElementGuid>a448cc57-ae57-4ce2-8a9b-0f0d019bf92f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;logisticsform&quot;)/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;form-group&quot;]/label[@class=&quot;error&quot;]</value>
      <webElementGuid>7e6073dc-19cf-4498-9ffd-9066f6beb983</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='logisticsform']/div/div/div/div/label[2]</value>
      <webElementGuid>381e77ce-08c2-458b-a3c1-aa6458937805</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logistic List'])[1]/following::label[1]</value>
      <webElementGuid>0528deef-a05f-4fe8-bc75-3a5de21387ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Logistice'])[1]/following::label[2]</value>
      <webElementGuid>b6faf965-39d3-4b52-81bf-e17414da0ede</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a Category'])[1]/preceding::label[1]</value>
      <webElementGuid>5d7693da-34e2-4641-9de5-957617cf389c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Project'])[2]/preceding::label[1]</value>
      <webElementGuid>b5154fff-b0a6-4ea4-9da8-0027b17c673f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='This field is required.']/parent::*</value>
      <webElementGuid>52c0c5ab-6b7f-4bba-ab3f-e0d03afdab48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label[2]</value>
      <webElementGuid>9857d5e2-e208-49d4-971b-f9d830676963</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'This field is required.' or . = 'This field is required.')]</value>
      <webElementGuid>3ef06153-11bc-43c9-9ab1-e940c5a01b7c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
