<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_This field is required Project</name>
   <tag></tag>
   <elementGuidId>0d577c68-c7a5-4172-9cfb-23fa5748eb32</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='logisticsform']/div/div/div/div[2]/label[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>0b6b11f5-559a-4786-aaf6-3e067eb91565</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>OnEmValue</value>
      <webElementGuid>4b8a9b1b-dadb-4f09-b45b-bc29e1e93656</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>generated</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>3f7cedee-f688-4f7c-9d2c-6461f95e297d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>error</value>
      <webElementGuid>8de0fc7c-d3ea-41fd-b89d-7ecb5c2b73a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>This field is required.</value>
      <webElementGuid>950d4393-e61d-4138-b252-b617eeb8474c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;logisticsform&quot;)/div[@class=&quot;modal-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-6&quot;]/div[@class=&quot;form-group&quot;]/label[@class=&quot;error&quot;]</value>
      <webElementGuid>43e549e2-6c52-4e99-b7a1-7a59c8ea50be</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='logisticsform']/div/div/div/div[2]/label[2]</value>
      <webElementGuid>2ddb07a8-fce7-4ce8-9e18-2fe79b72ede7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Project'])[2]/following::label[1]</value>
      <webElementGuid>2bc04d2b-0493-40ab-9303-6a42e979dfb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a Category'])[1]/following::label[2]</value>
      <webElementGuid>bb815a1a-7302-4771-915a-f09b9a08f150</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Choose a Category'])[2]/preceding::label[1]</value>
      <webElementGuid>f5042e07-5aa6-49b8-9acd-f3708c0236a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Task List'])[2]/preceding::label[1]</value>
      <webElementGuid>39724ed8-74b3-46ee-ab02-5f4963459c43</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/label[2]</value>
      <webElementGuid>ced4ea53-d694-43ec-9705-d2c51d072991</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'This field is required.' or . = 'This field is required.')]</value>
      <webElementGuid>ffc53810-5c42-42b3-98db-69ef0ad70bdb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
