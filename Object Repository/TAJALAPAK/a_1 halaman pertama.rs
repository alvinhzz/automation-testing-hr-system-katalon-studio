<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_1 halaman pertama</name>
   <tag></tag>
   <elementGuidId>2b2652db-03fe-4cf7-82ce-5136c22fbd1d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.paginate_button.active > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='example1_paginate']/ul/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>06eb35a4-fbdf-4c2c-9223-82b1f6ea252a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>d1be970d-d990-497a-adfd-9e339cd7abe4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>example1</value>
      <webElementGuid>efcc5e59-791b-47f3-91a1-5f4ca544890a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dt-idx</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>5a7f644b-a8d4-4cf4-b1c4-b70cebac2074</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>f3a8e29a-dce1-49a9-bfff-3cd369be5919</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>03aed126-e7fc-4741-8ad4-ef3b60cc4c83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example1_paginate&quot;)/ul[@class=&quot;pagination&quot;]/li[@class=&quot;paginate_button active&quot;]/a[1]</value>
      <webElementGuid>e8b107c4-2ee7-4d51-b96d-1d042c465be8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='example1_paginate']/ul/li[2]/a</value>
      <webElementGuid>8c86abce-589c-4bef-9ec2-0345867f1e98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'1')]</value>
      <webElementGuid>bc2c3c42-a5bb-4b2d-8357-65efc57947df</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Showing 1 to 10 of 71 entries'])[1]/following::a[2]</value>
      <webElementGuid>add4a992-9037-4c7c-b360-ab3a79ff66c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Published'])[9]/following::a[5]</value>
      <webElementGuid>357be703-6f18-41e2-9c6e-8b6f8ecafd82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Marketplace PRO'])[1]/preceding::a[8]</value>
      <webElementGuid>7f09f1b0-6bd5-41b4-b935-95edef8107b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[12]</value>
      <webElementGuid>8e115697-b353-45f6-98d4-f498161863fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/ul/li[2]/a</value>
      <webElementGuid>15f41507-d0a6-42d6-b662-187b1e106db4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = '1' or . = '1')]</value>
      <webElementGuid>619b1bd7-e831-4be6-b269-c6f64bff81f7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
