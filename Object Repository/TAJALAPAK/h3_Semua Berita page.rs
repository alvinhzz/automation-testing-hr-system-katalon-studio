<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_Semua Berita page</name>
   <tag></tag>
   <elementGuidId>c041025f-532e-4c37-83b6-70ae143789e5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h3.box-title</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Control panel'])[1]/following::h3[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>03f8344d-032f-4a9e-9c07-7688cb01f042</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>box-title</value>
      <webElementGuid>5a0aa5d5-d6f3-4225-b7e0-1f5917c305aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Semua Berita</value>
      <webElementGuid>308ae4e3-5caa-4fd9-a784-035ed93edb71</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;active&quot;]/body[@class=&quot;skin-black-light sidebar-mini  active pace-done&quot;]/div[@class=&quot;wrapper active&quot;]/div[@class=&quot;content-wrapper&quot;]/section[@class=&quot;content&quot;]/div[@class=&quot;col-xs-12&quot;]/div[@class=&quot;box&quot;]/div[@class=&quot;box-header&quot;]/h3[@class=&quot;box-title&quot;]</value>
      <webElementGuid>a9354fde-4986-49d5-8e87-b093104db857</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Control panel'])[1]/following::h3[1]</value>
      <webElementGuid>dd0a7b6f-66a7-44c1-815b-b5285aa55a7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambahkan Data'])[1]/preceding::h3[1]</value>
      <webElementGuid>5f186247-3d7e-447f-9cce-c3fdc3bb71aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Semua Berita']/parent::*</value>
      <webElementGuid>73a2ec68-a2ab-46f8-b00d-e210efeb9a16</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>f5944453-ebdd-4e3a-90a9-17173735c38a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'Semua Berita' or . = 'Semua Berita')]</value>
      <webElementGuid>91041012-275d-47cd-baf8-2513c0e3e9af</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
