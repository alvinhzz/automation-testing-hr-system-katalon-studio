<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_designition</name>
   <tag></tag>
   <elementGuidId>4ac5eedc-957f-40f6-937b-4cc497e4ba48</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;deg&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='deg']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>fe6b1fb4-d3a1-4402-94d7-a845a2db6c69</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>deg</value>
      <webElementGuid>96b9ebf9-44b7-46d4-af3f-4d330a0d839c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control custom-select</value>
      <webElementGuid>4fcd0239-0aba-4720-b90c-f11be736baa7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				                                            Sr. Finance &amp; Admin Officer - I
                                                                                        Vice Chairman
                                                                                        Chief Executive Officer (CEO)
                                                                                        Chief Finance &amp; Admin Officer
                                                                                        Sr. Finance &amp; Admin Officer - I
                                                                                        Jr. Finance &amp; Admin Officer
                                                                                        Senior Research Associate-1
                                                                                        Research Associate-1
                                                                                        Junior Research Associate
                                                                                        Research Assistant
                                                                                        Sr. Office Assistant
                                                                                        Office Assistant
                                                                                        IT Analyst
                                                                                        Cook
                                                                                        Software Engineer
                                                                                        System Analyst
                                                                                        Programmer Analyst
                                                                                        Sr Software Engineer
                                                                                        Technical Specialist
                                                                                        Trainee Engineer
                                                                                        Intern
                                                                                        Head of Department
                                                                                        Business Analyst
                                                                                        Web Developer
                                                                                        Big Data Engineer
                                                                                        Project Manager
                                                                                        Trainee
                                            				                                        </value>
      <webElementGuid>3701102a-ddaa-42ac-9f23-267732a90c89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;home&quot;)/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8&quot;]/form[@class=&quot;row&quot;]/div[@class=&quot;form-group col-md-4 m-t-10&quot;]/select[@class=&quot;form-control custom-select&quot;]</value>
      <webElementGuid>ef8b289f-bf8e-4c74-9fc9-b49cbbf813ba</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='deg']</value>
      <webElementGuid>7b730e9f-1f3a-44e3-9588-240364aa522a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='home']/div/div/div/div[2]/form/div[12]/select</value>
      <webElementGuid>8e8a14a5-8911-4b1e-a56d-2eebf781b7a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Designation'])[2]/following::select[1]</value>
      <webElementGuid>190e4f34-a8a2-431f-8c7e-8a1efc55c7b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Department'])[2]/following::select[2]</value>
      <webElementGuid>f2f7629f-e97a-45a9-8cb1-465c4a497dae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date Of Joining'])[1]/preceding::select[1]</value>
      <webElementGuid>f6385c49-da8a-4246-94f5-ceee698b6815</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contract End Date'])[1]/preceding::select[1]</value>
      <webElementGuid>bce3da06-f352-42f4-bd84-a48e7974e4a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/select</value>
      <webElementGuid>1205b53c-8f49-486b-9cda-0ae4c6921d6d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'deg' and (text() = '
				                                            Sr. Finance &amp; Admin Officer - I
                                                                                        Vice Chairman
                                                                                        Chief Executive Officer (CEO)
                                                                                        Chief Finance &amp; Admin Officer
                                                                                        Sr. Finance &amp; Admin Officer - I
                                                                                        Jr. Finance &amp; Admin Officer
                                                                                        Senior Research Associate-1
                                                                                        Research Associate-1
                                                                                        Junior Research Associate
                                                                                        Research Assistant
                                                                                        Sr. Office Assistant
                                                                                        Office Assistant
                                                                                        IT Analyst
                                                                                        Cook
                                                                                        Software Engineer
                                                                                        System Analyst
                                                                                        Programmer Analyst
                                                                                        Sr Software Engineer
                                                                                        Technical Specialist
                                                                                        Trainee Engineer
                                                                                        Intern
                                                                                        Head of Department
                                                                                        Business Analyst
                                                                                        Web Developer
                                                                                        Big Data Engineer
                                                                                        Project Manager
                                                                                        Trainee
                                            				                                        ' or . = '
				                                            Sr. Finance &amp; Admin Officer - I
                                                                                        Vice Chairman
                                                                                        Chief Executive Officer (CEO)
                                                                                        Chief Finance &amp; Admin Officer
                                                                                        Sr. Finance &amp; Admin Officer - I
                                                                                        Jr. Finance &amp; Admin Officer
                                                                                        Senior Research Associate-1
                                                                                        Research Associate-1
                                                                                        Junior Research Associate
                                                                                        Research Assistant
                                                                                        Sr. Office Assistant
                                                                                        Office Assistant
                                                                                        IT Analyst
                                                                                        Cook
                                                                                        Software Engineer
                                                                                        System Analyst
                                                                                        Programmer Analyst
                                                                                        Sr Software Engineer
                                                                                        Technical Specialist
                                                                                        Trainee Engineer
                                                                                        Intern
                                                                                        Head of Department
                                                                                        Business Analyst
                                                                                        Web Developer
                                                                                        Big Data Engineer
                                                                                        Project Manager
                                                                                        Trainee
                                            				                                        ')]</value>
      <webElementGuid>c3ca9e48-830e-4b0d-89c7-a328b0da6f8e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
