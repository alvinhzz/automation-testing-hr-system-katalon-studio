<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Employee List</name>
   <tag></tag>
   <elementGuidId>2c212976-571d-445d-b7f4-46313029bc9c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'm-b-0 text-white' and (text() = ' Employee List' or . = ' Employee List')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h4.m-b-0.text-white</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-wrapper']/div/div[3]/div[2]/div/div/div/h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>f9b1e60d-76df-4795-86d6-c68eef879697</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>m-b-0 text-white</value>
      <webElementGuid>eff326d3-6dca-42f9-97a5-151f17b963a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Employee List</value>
      <webElementGuid>a4cb6faf-1af7-4098-8109-906f505c32c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-wrapper&quot;)/div[@class=&quot;page-wrapper&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;card card-outline-info&quot;]/div[@class=&quot;card-header&quot;]/h4[@class=&quot;m-b-0 text-white&quot;]</value>
      <webElementGuid>d1c484ee-6a44-4b7a-8515-ec42ac827e27</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-wrapper']/div/div[3]/div[2]/div/div/div/h4</value>
      <webElementGuid>c977cea7-8102-4754-82f9-9bbb191233f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Disciplinary List'])[1]/following::h4[1]</value>
      <webElementGuid>7afbc1f2-f278-4b04-9d81-2bdbce6d8794</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Employee'])[1]/following::h4[1]</value>
      <webElementGuid>c2dc7d62-cf44-48b0-9f73-79511bd8d3b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy'])[1]/preceding::h4[1]</value>
      <webElementGuid>09304532-172a-4954-9aee-387a6f033d8b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CSV'])[1]/preceding::h4[1]</value>
      <webElementGuid>bf453372-7bc5-430a-83f4-c7bc04a080bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Employee List']/parent::*</value>
      <webElementGuid>bd24dae3-8d01-4c43-abb5-ddf8e570f45e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/h4</value>
      <webElementGuid>086608ce-6d7d-4597-a4fc-72808d665193</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = ' Employee List' or . = ' Employee List')]</value>
      <webElementGuid>c78d8ef4-b610-4c97-91e6-9ff0a67bf2c5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
