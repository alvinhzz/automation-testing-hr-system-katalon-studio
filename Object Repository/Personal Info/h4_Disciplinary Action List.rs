<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h4_Disciplinary Action List</name>
   <tag></tag>
   <elementGuidId>847dae19-9fe3-4590-9f63-2f89c2e9e2ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h4.m-b-0.text-white</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-wrapper']/div[2]/div[2]/div[2]/div/div/div/h4</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h4</value>
      <webElementGuid>cb7349ae-6437-4d68-8e4e-54eee4c9281b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>m-b-0 text-white</value>
      <webElementGuid>5e3597bb-6573-4070-b917-718daaeac6b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Disciplinary Action List</value>
      <webElementGuid>2ed74d94-7cf2-4641-a292-775558e22196</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-wrapper&quot;)/div[@class=&quot;page-wrapper&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;card card-outline-info&quot;]/div[@class=&quot;card-header&quot;]/h4[@class=&quot;m-b-0 text-white&quot;]</value>
      <webElementGuid>f01f7770-f207-4b03-ac9d-ba1c731b5ece</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-wrapper']/div[2]/div[2]/div[2]/div/div/div/h4</value>
      <webElementGuid>54c4dcf7-951d-4a7c-9169-3926caa36f81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Employee List'])[1]/following::h4[1]</value>
      <webElementGuid>d4e4adca-9ef2-47f4-9eea-9c240551f5e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Disciplinary'])[1]/following::h4[1]</value>
      <webElementGuid>e1485e1f-c9bd-463d-9d38-ab6b3ba1ebde</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy'])[1]/preceding::h4[1]</value>
      <webElementGuid>4a3ebe11-4a01-4a52-af1a-2585c4a1f5b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CSV'])[1]/preceding::h4[1]</value>
      <webElementGuid>3db2f4b2-183e-4d25-a6d7-1106008b57b4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Disciplinary Action List']/parent::*</value>
      <webElementGuid>92cec3b1-3955-4418-acf6-d36ecb4a4afb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/h4</value>
      <webElementGuid>3a0247b5-776b-453c-a5e0-18dad19573cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h4[(text() = ' Disciplinary Action List' or . = ' Disciplinary Action List')]</value>
      <webElementGuid>5e1ab9ba-3e3c-4924-a778-6d174488a460</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
