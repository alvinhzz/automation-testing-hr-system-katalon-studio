<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>select_departement</name>
   <tag></tag>
   <elementGuidId>29c583b1-1a29-47eb-a954-27791bc1b0ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>select[name=&quot;dept&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//select[@name='dept']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>select</value>
      <webElementGuid>18599a93-f047-4332-8426-3dba222a3008</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>dept</value>
      <webElementGuid>41b263fa-8986-4fae-a7b9-4cb35ee86228</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control custom-select</value>
      <webElementGuid>9a9a873b-264c-4eb7-ae4c-88a0746dee67</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
				                                            Information Technology
                                                                                         Administration
                                                                                         Finance, HR, &amp; Admininstration
                                                                                         Research
                                                                                         Information Technology
                                                                                         Support
                                                                                         Network Engineering
                                                                                         Sales and Marketing
                                                                                         Helpdesk
                                                                                         Project Management
                                            				                                        </value>
      <webElementGuid>2515c0cd-7f6d-4270-ab62-38501910154c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;home&quot;)/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8&quot;]/form[@class=&quot;row&quot;]/div[@class=&quot;form-group col-md-4 m-t-10&quot;]/select[@class=&quot;form-control custom-select&quot;]</value>
      <webElementGuid>2fa70a76-3de6-4240-bb2c-8e6b96d2281a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//select[@name='dept']</value>
      <webElementGuid>f3d06a4b-86f9-4f22-9ad9-49dbb247305a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='home']/div/div/div/div[2]/form/div[11]/select</value>
      <webElementGuid>09f41a2e-42af-46ff-9b16-ec6e9626f0e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Department'])[2]/following::select[1]</value>
      <webElementGuid>5e60566d-250b-4952-8d67-45026904e783</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact Number'])[1]/following::select[1]</value>
      <webElementGuid>13d38bf1-83a4-4e83-aaef-7d9ae41bda31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Designation'])[2]/preceding::select[1]</value>
      <webElementGuid>f4ff0048-b9fd-41e4-a277-850fbb3fa602</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date Of Joining'])[1]/preceding::select[2]</value>
      <webElementGuid>881b90ba-f15c-44fc-86c1-41d7e89d6940</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[11]/select</value>
      <webElementGuid>080f32e4-a126-45de-a6a6-890747e1a1e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//select[@name = 'dept' and (text() = '
				                                            Information Technology
                                                                                         Administration
                                                                                         Finance, HR, &amp; Admininstration
                                                                                         Research
                                                                                         Information Technology
                                                                                         Support
                                                                                         Network Engineering
                                                                                         Sales and Marketing
                                                                                         Helpdesk
                                                                                         Project Management
                                            				                                        ' or . = '
				                                            Information Technology
                                                                                         Administration
                                                                                         Finance, HR, &amp; Admininstration
                                                                                         Research
                                                                                         Information Technology
                                                                                         Support
                                                                                         Network Engineering
                                                                                         Sales and Marketing
                                                                                         Helpdesk
                                                                                         Project Management
                                            				                                        ')]</value>
      <webElementGuid>9814bcc9-7c70-4465-bfe3-b18511aab89b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
