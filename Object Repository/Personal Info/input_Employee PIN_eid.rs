<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Employee PIN_eid</name>
   <tag></tag>
   <elementGuidId>adbf8772-57a0-46bc-a062-d51a292602fe</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;eid&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='eid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>80bd6518-be59-4114-9865-74840cb2c537</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>e689eb70-4f0a-47aa-ba74-35580a344ae1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control form-control-line</value>
      <webElementGuid>4271189a-7fee-496e-8b1b-92d0ef2ac722</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>ID</value>
      <webElementGuid>93699a44-ea24-430f-82e0-b2c42562653f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>eid</value>
      <webElementGuid>214828cb-f1f6-4ca8-8bbe-b9840e05c226</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>435e27af-cc9d-4229-a9ee-fddc71e93dd8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;home&quot;)/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8&quot;]/form[@class=&quot;row&quot;]/div[@class=&quot;form-group col-md-4 m-t-10&quot;]/input[@class=&quot;form-control form-control-line&quot;]</value>
      <webElementGuid>4de1ea5d-f67a-4be4-b8ed-e81bb927c70a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='eid']</value>
      <webElementGuid>5c6b676d-6121-44db-9fd0-6d5d24407d5a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='home']/div/div/div/div[2]/form/div/input</value>
      <webElementGuid>061e9035-e4fe-43d4-a93a-ea33507f6feb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>16ddf48b-67fb-405a-a012-9f778a99a011</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @placeholder = 'ID' and @name = 'eid']</value>
      <webElementGuid>1861291e-a4a0-4b99-9571-0aff6e923fb6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
