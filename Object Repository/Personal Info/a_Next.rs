<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Next</name>
   <tag></tag>
   <elementGuidId>15395a8b-1ad9-438b-94ff-3f004004e815</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#employees123_next</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='employees123_next']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>56864a0f-0549-4cdc-87fa-8e03d80143dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>paginate_button next</value>
      <webElementGuid>11c20d3f-6f18-4d19-9449-1d6c16327f37</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>employees123</value>
      <webElementGuid>ed4356fd-cb55-4770-9c50-83b1c2ea2c51</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dt-idx</name>
      <type>Main</type>
      <value>3</value>
      <webElementGuid>870c29c2-5465-4314-bcfb-47739782c4c9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>b0fc581e-fc88-48e0-acaf-78dae5fd4fed</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>employees123_next</value>
      <webElementGuid>55f9693d-ba8c-415e-9727-b22cd0bb76ab</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Next</value>
      <webElementGuid>71556b7c-9435-47d1-a857-4e15c36d8a32</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;employees123_next&quot;)</value>
      <webElementGuid>49d42c4e-68f7-4d1a-b7ab-b00670b61965</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='employees123_next']</value>
      <webElementGuid>38f26cb6-b1cd-435b-8e7a-e4fe816a58b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='employees123_paginate']/a[2]</value>
      <webElementGuid>2186f12c-ea5d-4ebe-97b0-221d67777094</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Next')]</value>
      <webElementGuid>a0970154-652a-4a23-ae78-178f27430c15</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Previous'])[1]/following::a[3]</value>
      <webElementGuid>ccdd75cb-45d1-4d1a-a831-201482adcf46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Showing 1 to 10 of 18 entries'])[1]/following::a[4]</value>
      <webElementGuid>abfdcf25-e5f8-4acd-91f7-c9abda6dbd0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::a[1]</value>
      <webElementGuid>beda1532-9a43-42b6-b178-27a462778bc3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Alt'])[1]/preceding::a[1]</value>
      <webElementGuid>1836384c-f468-454d-b547-b8ae6a2791ac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Next']/parent::*</value>
      <webElementGuid>ec9331d1-2681-4180-9553-1e9843182da8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/a[2]</value>
      <webElementGuid>82722289-1660-452b-9712-9a13946aa494</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'employees123_next' and (text() = 'Next' or . = 'Next')]</value>
      <webElementGuid>5f554471-67b0-4d39-9ff8-8afb1ae02940</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
