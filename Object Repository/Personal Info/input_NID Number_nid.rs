<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_NID Number_nid</name>
   <tag></tag>
   <elementGuidId>055d96a8-8cc7-48c8-9258-4925b48d3a36</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;nid&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='nid']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>ab976ea3-cef7-415b-be7b-6169b194b30b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>ab9c658d-1e14-4297-a812-d1283e13e902</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control valid</value>
      <webElementGuid>385fd4b1-da31-4fb3-a947-d2b513ab3058</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>NID Number</value>
      <webElementGuid>358b1e69-8607-48cf-b817-e079f4c2b652</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>nid</value>
      <webElementGuid>9c2ed4a0-1cca-4d8b-ab8e-118714d56c0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>-12345@xyz</value>
      <webElementGuid>6e188a71-2a6e-4f8d-a3f2-54e560be6dbb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>minlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>36100de3-64c3-4fb9-b3ec-dc21424698f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;home&quot;)/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8&quot;]/form[@class=&quot;row&quot;]/div[@class=&quot;form-group col-md-4 m-t-10&quot;]/input[@class=&quot;form-control valid&quot;]</value>
      <webElementGuid>4c9403e9-da89-4235-9c9b-4e74688ae0e9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='nid']</value>
      <webElementGuid>791581ff-f0ad-4f8a-967a-ec523ee8b4ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='home']/div/div/div/div[2]/form/div[9]/input</value>
      <webElementGuid>ec4fd877-bae1-4a31-8394-afa798376fb2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[9]/input</value>
      <webElementGuid>eca272d9-2066-45bc-9788-5f374edc57b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @placeholder = 'NID Number' and @name = 'nid']</value>
      <webElementGuid>33769282-c43f-4022-92de-03ec73ffee4e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
