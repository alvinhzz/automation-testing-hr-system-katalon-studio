<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Bank Holder Name</name>
   <tag></tag>
   <elementGuidId>0f195736-4f1f-47e4-99c0-94510664e194</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#bank > div.card > div.card-body > form.row > div.form-group.col-md-6.m-t-5 > label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='bank']/div/div/form/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>aca78d9b-8fa5-45b3-be2a-656f556f19d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Bank Holder Name</value>
      <webElementGuid>36458997-f2a2-4be1-affe-a855bbd5cbce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bank&quot;)/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[@class=&quot;row&quot;]/div[@class=&quot;form-group col-md-6 m-t-5&quot;]/label[1]</value>
      <webElementGuid>6d8c7ebe-d31e-440d-8e34-26aa96eadd0f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='bank']/div/div/form/div/label</value>
      <webElementGuid>80f5bba4-d0d4-478a-91ba-b394f03b12a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Working Duration'])[1]/following::label[1]</value>
      <webElementGuid>0b7179b1-595d-423e-b6a9-91200ef4a182</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank Name'])[1]/preceding::label[1]</value>
      <webElementGuid>5e6aadfb-8858-4a9f-8cfc-b9b4bf33b6f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Branch Name'])[1]/preceding::label[2]</value>
      <webElementGuid>246a44b3-29d2-47f5-b670-7886aaa4a8cc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bank Holder Name']/parent::*</value>
      <webElementGuid>c4dd1c55-50f2-46d0-bd13-e893efd5d5f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/form/div/label</value>
      <webElementGuid>08a1c565-9c61-4451-9a4e-b356d39c0d6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = ' Bank Holder Name' or . = ' Bank Holder Name')]</value>
      <webElementGuid>a31c753b-afeb-4760-81ea-8f7114416daa</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
