<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Remember Me_password</name>
   <tag></tag>
   <elementGuidId>8115daa7-87b7-4724-ae9e-0d1c600d8148</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;password&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='password']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>db7fd665-1576-4218-9e76-8c4e890da855</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>65069e92-a43a-40f9-b4f7-20d04bd57874</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>e06e1008-570a-4485-825d-2788ee79c576</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>f89fb9c6-bf03-4076-9471-decbe9b2af5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Password</value>
      <webElementGuid>bc51a7e8-bbf7-49c5-9558-b508c939b8a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;loginform&quot;)/div[@class=&quot;form-group&quot;]/div[@class=&quot;col-xs-12&quot;]/input[@class=&quot;form-control&quot;]</value>
      <webElementGuid>0401c8c2-fb4a-40df-80f8-4f8c8297c145</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='password']</value>
      <webElementGuid>80acd713-73ab-4d3b-b1d9-01ab70602547</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='loginform']/div[2]/div/input</value>
      <webElementGuid>82592e84-ab14-4351-8b01-c53b239a030d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/input</value>
      <webElementGuid>8a03b879-a3db-4f6f-a324-75a2d2964e40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@name = 'password' and @type = 'password' and @placeholder = 'Password']</value>
      <webElementGuid>b56edd9c-b564-46dd-9386-e34df54cff8f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
