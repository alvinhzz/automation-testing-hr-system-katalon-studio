<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Employee PIN</name>
   <tag></tag>
   <elementGuidId>4b03b81b-b4be-4cbe-964f-817368279f17</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='home']/div/div/div/div[2]/form/div/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>0d30215f-f87f-4456-a37c-d81cd3591da3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Employee PIN </value>
      <webElementGuid>9a7fe3b5-6dc6-496d-ab41-fd00d7aa0752</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;home&quot;)/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8&quot;]/form[@class=&quot;row&quot;]/div[@class=&quot;form-group col-md-4 m-t-10&quot;]/label[1]</value>
      <webElementGuid>5b550a0d-8113-43ef-a3c8-85f5eda8d104</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='home']/div/div/div/div[2]/form/div/label</value>
      <webElementGuid>601a0f5f-17e0-4652-977c-d12deec17ba0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Social Profile'])[1]/following::label[1]</value>
      <webElementGuid>bed18ffa-ded4-400f-aefe-0ed16ac92899</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone'])[1]/following::label[1]</value>
      <webElementGuid>3178c3fc-7d90-4cfd-a536-d9953ec96944</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First Name'])[1]/preceding::label[1]</value>
      <webElementGuid>ceb17c4b-6735-42f6-a5ba-cad79638747f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Name'])[1]/preceding::label[2]</value>
      <webElementGuid>a88a623d-dd93-472e-8feb-93042ddd5acf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Employee PIN']/parent::*</value>
      <webElementGuid>cdb06d81-5de2-4bea-8cc8-b1dbec0eaafd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label</value>
      <webElementGuid>3bb664a1-e79a-43dc-8312-34595080333d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Employee PIN ' or . = 'Employee PIN ')]</value>
      <webElementGuid>ae6eecd8-7efe-4017-9f0f-4c1e0ea59265</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
