<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>edit bank account button</name>
   <tag></tag>
   <elementGuidId>2a00695e-a28a-4f64-997f-852787488a12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'nav-link active' and @href = '#bank' and @aria-expanded = 'true' and (text() = ' Bank Account' or . = ' Bank Account')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.nav-link.active</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-wrapper']/div/div[3]/div/div/div/ul/li[5]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6517b4bd-2fce-408e-96fe-7a3304514191</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link active</value>
      <webElementGuid>8ce8791f-19a2-4adc-9c7f-c8c43c684a77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>4dc67b9c-7c73-4732-8ca5-2451246693c1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#bank</value>
      <webElementGuid>06ab1491-61df-4256-9de4-f3975e87a58c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>tab</value>
      <webElementGuid>c3b89f7e-a22d-46f7-ae5f-563e6ba47cfc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>079301b6-cc04-4483-9393-b7bb52905625</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Bank Account</value>
      <webElementGuid>d1f8a2d6-a73f-479d-8b28-66c5aa51a768</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-wrapper&quot;)/div[@class=&quot;page-wrapper&quot;]/div[@class=&quot;container-fluid&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12 col-xlg-12 col-md-12&quot;]/div[@class=&quot;card&quot;]/ul[@class=&quot;nav nav-tabs profile-tab&quot;]/li[@class=&quot;nav-item&quot;]/a[@class=&quot;nav-link active&quot;]</value>
      <webElementGuid>376955e8-f779-4b41-8299-1f107df755d9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-wrapper']/div/div[3]/div/div/div/ul/li[5]/a</value>
      <webElementGuid>aa71e0f0-e54f-433e-a1a7-00e52e1ca65f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Bank Account')]</value>
      <webElementGuid>d03fe68a-6b96-4c69-8d6c-171e1f621ea3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Experience'])[1]/following::a[1]</value>
      <webElementGuid>3f058d5d-e5d8-4ac5-9f0a-6b0bb99e4420</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Education'])[1]/following::a[2]</value>
      <webElementGuid>460d479c-3e00-4c47-8098-dbd237f2ad4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Document'])[1]/preceding::a[1]</value>
      <webElementGuid>ce31dc00-2949-4383-ac40-cd74290111ee</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Salary'])[1]/preceding::a[2]</value>
      <webElementGuid>f312579d-2235-439c-be0c-56ea5b5cd34a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Bank Account']/parent::*</value>
      <webElementGuid>37cc2629-b410-4a26-b696-6b02da01e743</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#bank')]</value>
      <webElementGuid>3f9b5881-84ca-4a98-abc6-373888944d6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/ul/li[5]/a</value>
      <webElementGuid>c3e52668-5251-4e75-ad28-446f95b6c3c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#bank' and (text() = ' Bank Account' or . = ' Bank Account')]</value>
      <webElementGuid>fe444bc3-adf6-45b2-9102-b8298887fb68</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
