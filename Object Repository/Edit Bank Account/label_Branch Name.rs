<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_Branch Name</name>
   <tag></tag>
   <elementGuidId>2b4975e5-2c7c-4b9e-8749-d47dd4e3a08b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='bank']/div/div/form/div[3]/label</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>0e62a7a9-718d-43ad-a66e-0d310a18bde0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Branch Name</value>
      <webElementGuid>315e40a1-c122-4012-9dbb-79c470938792</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bank&quot;)/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[@class=&quot;row&quot;]/div[@class=&quot;form-group col-md-6 m-t-5&quot;]/label[1]</value>
      <webElementGuid>cd854a5c-f433-4fc7-b69e-3f4975616e65</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='bank']/div/div/form/div[3]/label</value>
      <webElementGuid>ee613505-0047-4022-aeb1-90af371a3294</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank Name'])[1]/following::label[1]</value>
      <webElementGuid>033778f3-5f5f-45a4-9e7d-df19daa73b1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank Holder Name'])[1]/following::label[2]</value>
      <webElementGuid>4c3f6421-f9b5-4b8d-bb9b-0acd2e460745</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank Account Number'])[1]/preceding::label[1]</value>
      <webElementGuid>1e417e34-2f68-4e7b-b8bf-2b8ed188ce41</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank Account Type'])[1]/preceding::label[2]</value>
      <webElementGuid>8ce654ae-aad6-4a66-a8b0-028f7e41b865</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Branch Name']/parent::*</value>
      <webElementGuid>0d895865-a795-4d39-be6a-37cce13d17a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/form/div[3]/label</value>
      <webElementGuid>b1842617-5ad1-4754-b3c7-cbd300c7bfbc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Branch Name' or . = 'Branch Name')]</value>
      <webElementGuid>b186cc4f-3427-41dc-bd8c-b4726bd45c4c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
