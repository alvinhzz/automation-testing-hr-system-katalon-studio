<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_CSV</name>
   <tag></tag>
   <elementGuidId>5ba1edaa-b66a-499b-9260-abef7a966fac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.dt-button.buttons-csv.buttons-html5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='example23_wrapper']/div/a[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7fa80662-e9b4-4849-a984-c1a8e221fb89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dt-button buttons-csv buttons-html5</value>
      <webElementGuid>f22a8499-5cf9-48b6-b241-94f5bd8dda82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>090d1a87-edbc-4ce2-8e08-9b9a05740550</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>example23</value>
      <webElementGuid>de9a071d-e193-4ff1-b713-3b6f7273e2be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>d1311c9f-4e64-407b-98ec-ae3e3ce19b42</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CSV</value>
      <webElementGuid>1527fd3e-5899-4973-821a-d9be77f4d8a0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23_wrapper&quot;)/div[@class=&quot;dt-buttons&quot;]/a[@class=&quot;dt-button buttons-csv buttons-html5&quot;]</value>
      <webElementGuid>54060b77-dd33-4333-a8ae-1c8226e56c14</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='example23_wrapper']/div/a[2]</value>
      <webElementGuid>92b3e2a4-50e9-40ca-a441-3b73bc209093</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy'])[1]/following::a[1]</value>
      <webElementGuid>1c398a89-816b-41ac-b3df-ea1b6e48b9f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notice'])[2]/following::a[2]</value>
      <webElementGuid>d2e3360b-44cf-4c36-a529-2177984cb775</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Excel'])[1]/preceding::a[1]</value>
      <webElementGuid>f6f11630-0c21-49b3-a729-1f3050137744</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[12]</value>
      <webElementGuid>d6e0dbec-27cb-4109-b48d-e65452340b36</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/a[2]</value>
      <webElementGuid>2e5963b0-176d-4b2d-8a75-95268187f063</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'CSV' or . = 'CSV')]</value>
      <webElementGuid>1da05d0b-a0d7-4815-b8d0-826be43aae4d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
