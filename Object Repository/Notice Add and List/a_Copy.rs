<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Copy</name>
   <tag></tag>
   <elementGuidId>1e02ddd9-254c-4ca3-bb9a-458e87c6a8d9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.dt-button.buttons-copy.buttons-html5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='example23_wrapper']/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6040028e-a8c3-4c65-b612-890896fded39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dt-button buttons-copy buttons-html5</value>
      <webElementGuid>cfd5fc1d-d79d-4122-a47f-ec4c0efb54e3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
      <webElementGuid>4f5ebbfa-070d-41cc-a3be-3933d7deb709</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>example23</value>
      <webElementGuid>3434de29-f9ee-4d3f-8b55-5b9605175808</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>f38907ec-0b55-4b96-9fa4-2d6c5463f97e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Copy</value>
      <webElementGuid>24da5069-d45a-48b6-ae08-5cc52d7508ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23_wrapper&quot;)/div[@class=&quot;dt-buttons&quot;]/a[@class=&quot;dt-button buttons-copy buttons-html5&quot;]</value>
      <webElementGuid>2a48b8a8-68f4-44d1-91e6-0bae1c6e3147</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='example23_wrapper']/div/a</value>
      <webElementGuid>18da3324-da6d-43a0-b0c3-413230ae2e29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Notice'])[2]/following::a[1]</value>
      <webElementGuid>f89130c5-3a35-4b7b-8dec-2a8e12d0ae5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Add Notice'])[1]/following::a[1]</value>
      <webElementGuid>85731c6a-8106-4528-a41f-8653939df1ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='CSV'])[1]/preceding::a[1]</value>
      <webElementGuid>8af24d8b-32e9-4947-8010-997e3e1ae9a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '#')])[11]</value>
      <webElementGuid>1183d305-e59f-414b-aa64-4d910ca2470d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/a</value>
      <webElementGuid>e6b61daa-b19f-428a-8576-e1ed67e37bd3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = 'Copy' or . = 'Copy')]</value>
      <webElementGuid>57333251-e805-4e87-8172-1cdcb86cbffb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
