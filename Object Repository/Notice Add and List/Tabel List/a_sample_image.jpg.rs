<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_sample_image.jpg</name>
   <tag></tag>
   <elementGuidId>e6c69a05-b876-4db2-9d7f-f8fd490bc765</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td:nth-of-type(3) > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='example23']/tbody/tr/td[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>8e509bf3-11f7-416c-b9ae-40d13fe407fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://10.9.2.63/hrsystem/assets/images/notice/sample_image.jpg</value>
      <webElementGuid>0d4cb4bf-3fbc-40ad-aa4e-c4c72b547aa0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
      <webElementGuid>436c6c04-a45f-4574-bada-26d58dd6c322</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>sample_image.jpg</value>
      <webElementGuid>8f93fbe8-6fb1-49e2-9013-0879d82b2600</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23&quot;)/tbody[1]/tr[@class=&quot;odd&quot;]/td[3]/a[1]</value>
      <webElementGuid>67b113cb-8c2f-416f-a121-72888c26a19e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='example23']/tbody/tr/td[3]/a</value>
      <webElementGuid>67bb3e90-e728-4073-ae60-784400a805ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'sample_image.jpg')]</value>
      <webElementGuid>e71442f6-818e-4aae-85ef-48ca6d7eb037</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This is a demo notice for all!'])[1]/following::a[1]</value>
      <webElementGuid>7b0170da-d6e8-4e87-adbd-c7b97dd3c98b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date'])[1]/following::a[1]</value>
      <webElementGuid>4b85b068-975c-4b9f-ab91-619ab39186fc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Office Decorum Notice to Staff Members'])[1]/preceding::a[1]</value>
      <webElementGuid>be66d8bb-5a51-4da3-aa84-7a0c1aabc8e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='offnot1.png'])[1]/preceding::a[1]</value>
      <webElementGuid>bf554c54-770d-4327-b241-bb22b9ff6ad0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='sample_image.jpg']/parent::*</value>
      <webElementGuid>f79d80f8-c285-45bc-b37a-9ccc16df18cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://10.9.2.63/hrsystem/assets/images/notice/sample_image.jpg')]</value>
      <webElementGuid>97a58bf6-d37e-4819-ad89-fcb86252fe30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[3]/a</value>
      <webElementGuid>752b6234-3013-478d-ad23-fc236825a7c6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://10.9.2.63/hrsystem/assets/images/notice/sample_image.jpg' and (text() = 'sample_image.jpg' or . = 'sample_image.jpg')]</value>
      <webElementGuid>10d8cce4-344e-424e-894d-eb1e20a03b26</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
