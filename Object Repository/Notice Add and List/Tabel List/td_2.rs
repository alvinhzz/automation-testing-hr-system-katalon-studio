<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_2</name>
   <tag></tag>
   <elementGuidId>fbab2b42-9711-4d55-9cae-f635496d0719</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td:nth-of-type(2)</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='example23']/tbody/tr/td[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>c4e8ed72-086d-4eda-ae8c-8d4529bba312</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>This is a demo notice for all!</value>
      <webElementGuid>f8bfa16b-bfcc-4c88-a465-fe581d0d5a6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;example23&quot;)/tbody[1]/tr[@class=&quot;odd&quot;]/td[2]</value>
      <webElementGuid>98c8804a-25cc-4722-b261-d4d5bd174077</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='example23']/tbody/tr/td[2]</value>
      <webElementGuid>6b06b7ab-603f-4f4b-97a7-e628966dd92f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Date'])[1]/following::td[2]</value>
      <webElementGuid>27f5cb4d-6a9f-4072-936b-d0b413c402f3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='File'])[1]/following::td[2]</value>
      <webElementGuid>6d3a2835-a03e-4422-96e3-1c4e4096d61c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='sample_image.jpg'])[1]/preceding::td[1]</value>
      <webElementGuid>55842348-67e4-44ec-82da-52d3b01c9b89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Office Decorum Notice to Staff Members'])[1]/preceding::td[4]</value>
      <webElementGuid>a3120428-1e26-4b64-bb4b-438e0cc663e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='This is a demo notice for all!']/parent::*</value>
      <webElementGuid>83b9afab-5be1-4a6f-a85e-a9bc6a4dd012</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[2]</value>
      <webElementGuid>a6599af0-ec6b-4102-88d6-cce8ab06926d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = 'This is a demo notice for all!' or . = 'This is a demo notice for all!')]</value>
      <webElementGuid>51b5d660-b370-4d59-9383-a80889bb2c7d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
