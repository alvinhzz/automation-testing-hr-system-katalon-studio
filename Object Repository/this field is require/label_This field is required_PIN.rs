<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>label_This field is required_PIN</name>
   <tag></tag>
   <elementGuidId>eb4428aa-d67c-474f-8c6d-d4f4fab6f720</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>label.error</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='home']/div/div/div/div[2]/form/div/label[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>bfe44b94-6ef9-4164-870b-21f9434b336c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>for</name>
      <type>Main</type>
      <value>eid</value>
      <webElementGuid>7179d467-b879-45ca-9150-0f3cec7737a1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>generated</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e8490757-1b11-4621-8e13-3589a99e3302</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>error</value>
      <webElementGuid>08093ad8-61a7-4d87-aa08-004a6414e40f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>This field is required.</value>
      <webElementGuid>40ad3d8c-d985-4f73-9630-ca1e2c61f43d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;home&quot;)/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-8&quot;]/form[@class=&quot;row&quot;]/div[@class=&quot;form-group col-md-4 m-t-10&quot;]/label[@class=&quot;error&quot;]</value>
      <webElementGuid>f79e0b13-8519-4519-ba74-cb4e5bb78426</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='home']/div/div/div/div[2]/form/div/label[2]</value>
      <webElementGuid>d05e1472-e372-44b4-bd7f-4e88f954b1b6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Employee PIN'])[1]/following::label[1]</value>
      <webElementGuid>659915fe-e807-4742-9e05-3c210a87a503</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Social Profile'])[1]/following::label[2]</value>
      <webElementGuid>8771e80a-67a4-43e9-906e-c99d6bba4ed1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First Name'])[1]/preceding::label[1]</value>
      <webElementGuid>613d77a4-5f92-4078-8e24-6782eb5944ff</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='This field is required.'])[2]/preceding::label[2]</value>
      <webElementGuid>68202337-c938-47f3-b984-889113ee9c5e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='This field is required.']/parent::*</value>
      <webElementGuid>6ef5a622-2caa-47b1-ba5b-14819642bdaf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label[2]</value>
      <webElementGuid>fde0f52f-37a2-479f-b810-54b3360d983f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'This field is required.' or . = 'This field is required.')]</value>
      <webElementGuid>d8ecace6-1522-4d38-8de1-fd250471cd35</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
